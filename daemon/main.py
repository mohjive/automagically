#!/usr/bin/env python
import sys
import time
import os
import stat
import subprocess

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'site', 'plugins')

sys.path.append(PLUGINS_PATH)

sys.path.append('/home/pi/django_site/automagically')

os.environ['DJANGO_SETTINGS_MODULE'] = 'ha.settings_apache'
os.environ['AUTOMAGICALLY_SIGNALHANDLING'] = 'Y'

from daemon import Daemon
import signals.models
from core.models import RawTellstickDevice
import plugins
from django.conf import settings

MIGRATIONS = ['python /home/pi/source/automagically/site/manage.py migrate core',
              'python /home/pi/source/automagically/site/manage.py migrate signals',
              'python /home/pi/source/automagically/site/manage.py migrate settings',
              'python /home/pi/source/automagically/site/manage.py migrate remote']

STORAGEENGINE = 'mysql --verbose --user=' + settings.DATABASES['default']['USER'] + ' --password=' + settings.DATABASES['default']['PASSWORD'] + ' ' + settings.DATABASES['default']['NAME'] + ' < /home/pi/source/automagically/site/db_change_storage_engine.sql'


class Automagically(Daemon):
    def checkInitialCommands(self):
        try:
            fd = open(os.path.join(self.storage, 'init.cmd'), 'r')
        except:
            return

        fdlog = open(os.path.join(settings.LOG_ROOT, 'init.cmd.log'), 'w')
        for line in fd:
            if line.strip() != '':
                fdlog.write('**************************************\n')
                fdlog.write('* Executing: ' + line.strip() + '\n')
                fdlog.flush()

                if line.strip() == 'DELETE TELLSTICK DEVICES':
                    fdlog.write('Delete all current tellstick devices\n')
                    for t in RawTellstickDevice.objects.all():
                        fdlog.write('  ' + str(t) + '\n')
                        t.delete()
                        #Maybe should do a delete of all devices reported by telldus as well here

                elif line.strip() == 'SAVE TELLSTICK DEVICES':
                    fdlog.write('Clean+Save all current tellstick devices\n')  
                    for t in RawTellstickDevice.objects.all():
                        t.deviceId = 9999
                        t.clean()
                        t.save()
                        fdlog.write('  ' + str(t) + '\n')
                else:
                    p = subprocess.Popen(line.strip(), stdout = subprocess.PIPE, stderr = subprocess.STDOUT, cwd = self.storage, shell=True)
                    fdlog.write(p.stdout.read())
                    fdlog.flush()
                    p.wait()

        fdlog.close()                
        fd.close()

        fd = open(os.path.join(self.storage, 'init.cmd'), 'w')
        fd.close()

    def waitMysql(self):
        cmd = 'mysql --user=' + settings.DATABASES['default']['USER'] + ' --password=' + settings.DATABASES['default']['PASSWORD'] + ' ' + settings.DATABASES['default']['NAME'] + ' < /dev/null'

        while 1:
            p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
            if p.wait() == 0:
                break
            else:
                time.sleep(5)

    def processSignal(self, signal):
        print 'Process signal', signal

        if signal.content.startswith('system,dbrestore:'):
            print 'Restore database at next init'
            backupFile = signal.content[len('system,dbrestore:'):].strip()
            print backupFile
            cmd = 'mysql --verbose --user=' + settings.DATABASES['default']['USER'] + ' --password=' + settings.DATABASES['default']['PASSWORD'] + ' ' + settings.DATABASES['default']['NAME'] + ' < ' + backupFile

            fd = open(os.path.join(self.storage, 'init.cmd'), 'a')

            fd.write('DELETE TELLSTICK DEVICES\n')

            fd.write('sudo /etc/init.d/apache2 stop\n')

            fd.write(cmd + '\n')

            for cmd in MIGRATIONS:
                fd.write(cmd + '\n')

            fd.write(STORAGEENGINE + '\n')

            fd.write('SAVE TELLSTICK DEVICES\n')

            fd.write('sudo /etc/init.d/apache2 start\n')

            fd.close()
            

        elif signal.content.startswith('system,clearinit'):
            print 'Clear what to execute at next init'
            fd = open(os.path.join(self.storage, 'init.cmd'), 'w')
            fd.close()


        elif signal.content.startswith('system,update'):
            print 'Update'
            time.sleep(5)

            cmds = ['sudo /etc/init.d/apache2 stop']
            cmds.append('sudo /etc/init.d/mysql restart')
            cmds.append('git pull')
            cmds.extend(MIGRATIONS)
            cmds.append(STORAGEENGINE)
            cmds.append('python /home/pi/source/automagically/site/manage.py collectstatic --noinput')
            cmds.append('sudo shutdown -r now')
                        
            fdlog = open(os.path.join(settings.LOG_ROOT, 'update.log'), 'w')
            for cmd in cmds:
                fdlog.write('**************************************\n')
                fdlog.write('* Executing: ' + cmd + '\n')                

                p = subprocess.Popen(cmd, cwd = '/home/pi/source/automagically/', shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)

                fdlog.write(p.stdout.read())
                p.wait()

            fdlog.close()

        elif signal.content.startswith('system,reboot'):
            print 'Reboot'
            #Allways wait a lite lite before the actual reboot to make sure webpages are refreshed
            time.sleep(5)
            subprocess.Popen('sudo /etc/init.d/apache2 stop', shell = True).wait()
            subprocess.Popen('sudo shutdown -r now', shell = True).wait()

        elif signal.content.startswith('system,signalhandler,changed'):
            print 'Update handler cache'
            signals.models.fillHandlerCache()

        else:
            print 'Unknown command for system', signal.content


    def run(self):
        self.keepRunning = True
        self.storage = os.path.split(os.path.abspath(__file__))[0]

        if not os.path.exists(settings.LOG_ROOT):
            try:
                os.makedirs(settings.LOG_ROOT)
                print 'Created directory for log storage at:', settings.LOG_ROOT
            except:
                print 'Failed creating directory for log file storage', settings.LOG_ROOT

        print 'Wait for MySQL to start'
        self.waitMysql()

        print 'Mysql is running'

        self.checkInitialCommands()

        print 'Starting signalhandler'
        self.signalHandler = signals.models.SignalProcessingThread()
        self.signalHandler.start()

        print 'Starting plugins'
        plugins.startThreads()

        self.fifoNameInput = os.path.join(self.storage, 'daemon_input')
        self.fifoNameOutput = os.path.join(self.storage, 'daemon_output')

        try:
            while self.keepRunning:
#                print 'Accepting messages'
                self.fifoInput = open(self.fifoNameInput, 'r')
                msg = True
                while msg:
                    msg = self.fifoInput.readline()                
                    if msg:
#                        print 'Got message:', msg
                        s = signals.models.SignalObject(fromStr = msg)

                        if s.content.startswith('system'):
                            self.processSignal(s)

                        signals.models.signalQueue.put(s)
                        
                self.fifoInput.close()
        except:
            self.stopRunning()


    def stopRunning(self):
        self.keepRunning = False
        try:
            fd = open(self.fifoNameInput, 'w')
            fd.close()
        except:
            pass

        try:
            self.fifoInput.close()
        except:
            pass
        
        try:
            plugins.stopThreads()
        except:            
            pass

        try:
            self.signalHandler.stopThread()
        except:
            pass
                
 
if __name__ == "__main__":
    daemon = Automagically('/var/run/automagically.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
	elif 'stop' == sys.argv[1]:
            daemon.stop()
	elif 'restart' == sys.argv[1]:
            daemon.restart()
	elif 'foreground' == sys.argv[1]:
            daemon.run()
	else:
            print "Unknown command"
	    sys.exit(2)
	    sys.exit(0)

    elif len(sys.argv) == 1:
        daemon.run()        
    else:
        print "usage: %s start|stop|foreground" % sys.argv[0]
        sys.exit(2)
