# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StoreGlobalVariable'
        db.create_table('signals_storeglobalvariable', (
            ('handler_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['signals.Handler'], unique=True, primary_key=True)),
            ('ParsedVariable', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('Variable', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.GlobalVariable'])),
        ))
        db.send_create_signal('signals', ['StoreGlobalVariable'])

    def backwards(self, orm):
        # Deleting model 'StoreGlobalVariable'
        db.delete_table('signals_storeglobalvariable')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'htmlText': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'core.globalvariable': {
            'Meta': {'object_name': 'GlobalVariable'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastUpdated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'signals.devicecommand': {
            'Meta': {'object_name': 'DeviceCommand', '_ormbases': ['signals.Handler']},
            'cmd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'handler_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['signals.Handler']", 'unique': 'True', 'primary_key': 'True'})
        },
        'signals.handler': {
            'Meta': {'object_name': 'Handler'},
            'desc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pattern': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'patternIsRegularExpression': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'regexp': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'signals.storeglobalvariable': {
            'Meta': {'object_name': 'StoreGlobalVariable', '_ormbases': ['signals.Handler']},
            'ParsedVariable': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'Variable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']"}),
            'handler_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['signals.Handler']", 'unique': 'True', 'primary_key': 'True'})
        },
        'signals.transform': {
            'Meta': {'object_name': 'Transform', '_ormbases': ['signals.Handler']},
            'handler_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['signals.Handler']", 'unique': 'True', 'primary_key': 'True'}),
            'output': ('django.db.models.fields.TextField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['signals']