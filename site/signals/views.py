from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect, get_object_or_404, render
from signals.models import postToQueue, Transform, DeviceCommand, StoreGlobalVariable, FindRepeat
from core.views import getViewType
from core.models import Device, Command, GlobalVariable, RawTellstickDevice
from settings.models import getSettings, updateSetting
from django.conf import settings
from django import forms


import os
import re
from scanf import scanf_compile
import datetime
import time

debug = False

def send(request, inp):

    postToQueue(inp, 'httpreq', True)
    
    return render_to_response('admin/signals_index.html', {'viewType': getViewType(request), 'signal_str': inp})
    
def dump(request):
    fd = open(os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins', 'signaldebug.txt'), 'r')

    rawlines = fd.readlines(15000)
    fd.close()


    lines = []
    for r in rawlines:
        lines.append((r, r.split('#')[1].replace(' ', '+')))

    return render_to_response('admin/signals_index.html', {'viewType': getViewType(request), 'lines': lines})


def index(request):
    signal_str = ''

    if request.method == 'POST':
        form = SignalForm(request.POST)

        if form.is_valid():
            if debug:
                print request.POST

            if 'Send' in request.POST and request.POST['Send'] == 'Send':
                signal_str = form.cleaned_data['test_signal']
                postToQueue(signal_str, 'httpreq')

                signal_str += ' ' + datetime.datetime.now().isoformat(' ')

                time.sleep(1) #Wait and let the signal propagate

            elif 'Refresh' in request.POST and request.POST['Refresh'] == 'Refresh':
                pass


            elif 'SignalDebugToggle' in request.POST and request.POST['SignalDebugToggle'] == 'Toogle signal debug':
                print 'Toggle signal debug'

                signaldebug = getSettings('signaldebug')['Debug to file']

                if signaldebug:
                    signaldebug = False
                else:
                    signaldebug = True
                if not updateSetting('signaldebug', 'Debug to file', signaldebug):
                    print 'Failed updating setting to toggle signal debug to file'

    else:
        form = SignalForm()

    try:
        fd = open(os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins', 'signaldebug.txt'), 'r')

        rawlines = fd.readlines(15000)
        fd.close()
    except:
        print 'Error opening file'


    lines = []
    for r in rawlines:
        if r.strip() != '':
            try:
                date, sender, signal = r.split('#', 2)
                signal = signal.replace(' ', '+')

                if signal.startswith('tellstick,raw:class:command;'):
                    extraText = 'Tellstick device'
                    extraTarget = '/signals/create/tellstick/' + signal.split(':', 1)[1]
                else:
                    extraText = ''
                    extraTarget = ''

                lines.append((date, sender, signal, extraText, extraTarget))
            except:
                if debug:
                    print 'Ignored line from signal dump file:', r
                    raise


    signaldebug = getSettings('signaldebug')['Debug to file']

    return render(request,
                  'admin/signals_index.html',
                  {'form': form,
                   'signal_str': signal_str,
                   'lines': lines,
                   'signaldebug': signaldebug})


        
    

HANDLER_CHOICES = ((0, 'Transform'),
                   (1, 'Find repeated'),
                   (2, 'Store global variable'),
                   (3, 'Device command'))

class SignalForm(forms.Form):
    test_signal = forms.CharField(required=False, max_length=255, help_text = "Test signal to send in")

class CreateSignalHandlerForm(forms.Form):
    description = forms.CharField(max_length=100, help_text = "A short description of the signal handler")
    pattern = forms.CharField(max_length=255, help_text = "Pattern to match use scanf if you dont know what you doing (That is dont tick box below). To match a string just write the string if you want to match an integer add a %d instead of the number. If match a floating point number (it is requested to have a decimal delimiter) add a %f. To match a generic string add a %s.")
    patternIsRegularExpression = forms.BooleanField(required=False)
    handler = forms.ChoiceField(choices = HANDLER_CHOICES, widget = forms.RadioSelect())



class CreateTransformForm(forms.ModelForm):
    class Meta:
        model = Transform
        exclude = ('regexp',)

class CreateFindRepeatForm(forms.ModelForm):
    class Meta:
        model = FindRepeat
        exclude = ('regexp',)

class CreateDeviceCommandForm(forms.ModelForm):
    class Meta:
        model = DeviceCommand
        exclude = ('regexp',)

class CreateStoreGlobalVariableForm(forms.ModelForm):
    class Meta:
        model = StoreGlobalVariable
        exclude = ('regexp',)

    
def create(request, inp = None):
    heading = 'Create Signal handler'
    instructions = ''
    testdone = False
    parsed = []
    matched = False

    if request.method == 'POST':
        formType = request.session.get('formtype', '-1')

        signalForm = SignalForm(request.POST)

        if formType == '0':
            form = CreateTransformForm(request.POST)
            heading = 'Create Transform Signal Handler'

        elif formType == '1':
            form = CreateFindRepeatForm(request.POST)
            heading = 'Create Find Repeat Signal Handler'

        elif formType == '2':
            form = CreateStoreGlobalVariableForm(request.POST)
            heading = 'Create Store Global Variable Signal Handler'

        elif formType == '3':
            form = CreateDeviceCommandForm(request.POST)
            heading = 'Create Device Command Signal Handler'

        else:
            form = CreateSignalHandlerForm(request.POST)
            heading = 'Create Signal Handler'

        if form.is_valid() and signalForm.is_valid(): # All validation rules pass            

            if debug:
                print 'Form validated'
                print request.POST


            if not form.cleaned_data['patternIsRegularExpression']:
                regexp, _x = scanf_compile(form.cleaned_data['pattern'])
            else:
                regexp = form.cleaned_data['pattern']

            if 'Test' in request.POST and request.POST['Test'] == 'Test':
                testdone = True
                try:
                    regexp_comp = re.compile(regexp)
                    matched = regexp_comp.match(signalForm.cleaned_data['test_signal'])
                    if matched:
                        parsed = matched.groups()
                    else:
                        parsed = []
                except:
                    raise
                    
            elif 'Save' in request.POST and request.POST['Save'] == 'Save':
                handler = request.POST.get('handler', False)

                if handler:
                    request.session['formtype'] = handler
                    if handler == '0':
                        form = CreateTransformForm(initial={'desc': form.cleaned_data['description'],
                                                            'pattern': form.cleaned_data['pattern'],
                                                            'regexp': regexp,
                                                            'patternIsRegularExpression': form.cleaned_data['patternIsRegularExpression'],
                                                            'output': ''})

                    elif handler == '1':
                        form = CreateFindRepeatForm(initial={'desc': form.cleaned_data['description'],
                                                             'pattern': form.cleaned_data['pattern'],
                                                             'regexp': regexp,
                                                             'patternIsRegularExpression': form.cleaned_data['patternIsRegularExpression']})
                    
                    elif handler == '2':
                        form = CreateStoreGlobalVariableForm(initial={'desc': form.cleaned_data['description'],
                                                                      'pattern': form.cleaned_data['pattern'],
                                                                      'regexp': regexp,
                                                                      'patternIsRegularExpression': form.cleaned_data['patternIsRegularExpression']})
                                                                
                    elif handler == '3':
                        form = CreateDeviceCommandForm(initial={'desc': form.cleaned_data['description'],
                                                                'pattern': form.cleaned_data['pattern'],
                                                                'regexp': regexp,
                                                                'patternIsRegularExpression': form.cleaned_data['patternIsRegularExpression']})

                    else:
                        print 'Unknown handler'

                else:
                    #We are on second stage with ModelForm beeing submitted
                    newHandler = form.save()

                    if formType == '0':
                        return redirect('/admin/signals/transform/' + str(newHandler.id) + '/')
                    elif formType == '1':
                        return redirect('/admin/signals/findrepeat/' + str(newHandler.id) + '/')
                    elif formType == '2':
                        return redirect('/admin/signals/storeglobalvariable/' + str(newHandler.id) + '/')
                    elif formType == '3':
                        return redirect('/admin/signals/devicecommand/' + str(newHandler.id) + '/')
                    else:
                        print 'Unknown formType'
                        return redirect('/signals/create/')
                    
            else:
                print 'Dont know how to handle this request in /signals/create/'
                print request

    else:

        if inp == None:
            inp = ''

        request.session['formtype'] = '-1'
        form = CreateSignalHandlerForm(initial={'pattern': inp,
                                                'handler': 0,
                                                'description': 'Change my description'})
        signalForm = SignalForm(initial={'test_signal': inp})

    if debug:
        print 'testdone', testdone
        print 'matched', matched
        print 'parsed', parsed

    return render(request,
                  'admin/signals_create.html',
                  {'form': form,
                   'signalform': signalForm,
                   'viewType': getViewType(request),
                   'testdone': testdone,
                   'matched': matched,
                   'parsed': parsed,
                   'heading': heading,
                   'instructions': instructions})


class CreateRawTellstickDeviceForm(forms.ModelForm):
    class Meta:
        model = RawTellstickDevice
        exclude = ('activate', 'onOff', 'dim', 'deviceId')


def createtellstick(request, rawcommand = ''):
    if request.method == 'POST':
        form = CreateRawTellstickDeviceForm(request.POST)

        if form.is_valid():
            dev = form.save()
            return redirect('/admin/core/rawtellstickdevice/' + str(dev.id) + '/')            

    elif rawcommand != '':
        print 'rawcommand'
        print rawcommand

        args = rawcommand.split(';')
        initial = {'name': 'New Device', 'rawName': 'newdevice'}
        for a in args:
            try:
                arg, value = a.split(':', 1)                
                initial[arg] = value
            except:
                print 'Failed parsing', a
            


        if initial['protocol'] == 'arctech' and initial['model'] == 'selflearning':
            initial['protocol'] = initial['protocol'] + ';' + initial['model']+'-switch'
        else:
            initial['protocol'] = initial['protocol'] + ';' + initial['model']
        print initial
        form = CreateRawTellstickDeviceForm(initial = initial)

    else:
        form = CreateRawTellstickDeviceForm()

    return render(request,
                  'admin/signals_create_tellstick.html',
                  {'form': form,
                   'viewType': getViewType(request)})

