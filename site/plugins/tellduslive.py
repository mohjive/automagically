from django.db import models
import core.td
import signals.models
from core.models import Device, Command, CurrentValue
from settings.models import getSettings, updateSetting
import time
import Queue
import traceback
import sys

import httplib
import xml.parsers.expat

import socket, ssl
import time, os
import hashlib
import base64

PLUGIN_NAME = 'tellduslive'

debug = False

workQueue = Queue.Queue()

global currentSettings
currentSettings = {}

global localSettings
localSettings = {'hashMethod': 'sha1',
                 'Public key': '7AMUCHUDRUKAREBEXAFASW5CAR7R5GAT',
                 'Private key': 'KUBEBRAPR7GU5UY5QAVECRUQUSWARUFR',
                 'supportedMethods': 0}


class LiveMessageToken():
    TYPE_INVALID, TYPE_INT, TYPE_STRING, TYPE_BASE64, TYPE_LIST, TYPE_DICTIONARY = range(6)

    def __init__(self, value = None):
        self.valueType = LiveMessageToken.TYPE_INVALID
        self.stringVal = ''
        self.intVal = 0
        self.dictVal = {}
        self.listVal = []
        if (type(value) is int):
            self.valueType = self.TYPE_INT
            self.intVal = value

        elif (type(value) is str):
            self.valueType = self.TYPE_STRING
            self.stringVal = value

        elif (type(value) is unicode):
            self.valueType = self.TYPE_STRING
            self.stringVal = str(value)

        elif (type(value) is list):
            self.valueType = self.TYPE_LIST
            for v in value:
                self.listVal.append(LiveMessageToken(v))

        elif (type(value) is dict):
            self.valueType = self.TYPE_DICTIONARY
            for key in value:
                self.dictVal[key] = LiveMessageToken(value[key])

    def toStr(self):
        s = self.stringVal + ' ' + str(self.intVal) + ' '
        s += '{'
        for k in self.dictVal.keys():
            s += str(k) + ' ' + self.dictVal[k].toStr()
            s += '} '
            s += '['
        for e in self.listVal:
            s += e.toStr()
            s += ']'

        return s

    def toByteArray(self):
        if (self.valueType == LiveMessageToken.TYPE_INT):
            return 'i%Xs' % self.intVal
        
        if (self.valueType == LiveMessageToken.TYPE_LIST):
            retval = 'l'
            for token in self.listVal:
                retval = retval + token.toByteArray()
            return retval + 's'

        if (self.valueType == LiveMessageToken.TYPE_DICTIONARY):
            retval = 'h'
            for key in self.dictVal:
                retval = retval + LiveMessageToken(key).toByteArray() + self.dictVal[key].toByteArray()
            return retval + 's'

        return '%X:%s' % (len(self.stringVal), self.stringVal,)

    @staticmethod
    def parseToken(string, start):
        token = LiveMessageToken()
        if (start >= len(string)):
            return (start, token)

        if (string[start] == 'i'):
            start+=1
            index = string.find('s', start)
            if (index < 0):
                return (start, token)

            try:
                token.intVal = int(string[start:index], 16)
                token.valueType = LiveMessageToken.TYPE_INT
                start = index + 1
            except:
                return (start, token)

        elif (string[start] == 'l'):
            start+=1
            while (start < len(string) and string[start] != 's'):
                start, listToken = LiveMessageToken.parseToken(string, start)
                if (listToken.valueType == LiveMessageToken.TYPE_INVALID):
                    break
                token.valueType = LiveMessageToken.TYPE_LIST
                token.listVal.append(listToken)
                start+=1

        elif (string[start] == 'h'):
            start+=1
            while (start < len(string) and string[start] != 's'):
                start, keyToken = LiveMessageToken.parseToken(string, start)
                if (keyToken.valueType == LiveMessageToken.TYPE_INVALID):
                    break
                start, valueToken = LiveMessageToken.parseToken(string, start)
                if (valueToken.valueType == LiveMessageToken.TYPE_INVALID):
                    break
                token.valueType = LiveMessageToken.TYPE_DICTIONARY
                token.dictVal[keyToken.stringVal] = valueToken
            start+=1

        elif (string[start] == 'u'): #Base64
            start+=1
            start, token = LiveMessageToken.parseToken(string, start)
            token.valueType = LiveMessageToken.TYPE_BASE64
            token.stringVal = base64.decodestring(token.stringVal)

        else: #String
            index = string.find(':', start)

            if (index < 0):
                return (start, token)

            try:
                length = int(string[start:index], 16)
            except:
                return (start, token)

            start = index + length + 1
            token.stringVal = string[index+1:start]
            token.valueType = LiveMessageToken.TYPE_STRING
            
        return (start, token)



class LiveMessage():
    def __init__(self, name = ""):
        if (name != ""):
            self.args = [LiveMessageToken(name)]
        else:
            self.args = []

    def toStr(self):
        s = ''
        for a in self.args:
            s += a.toStr() + '\n'
        return s

    def append(self, argument):
        self.args.append(LiveMessageToken(argument))

    def argument(self,index):
        if (len(self.args) > index+1):
            return self.args[index+1]

        return LiveMessageToken()

    def count(self):
        return len(self.args)-1

    def name(self):
        return self.argument(-1).stringVal.lower()

    def toByteArray(self):
        retval = ''
        for arg in self.args:
            retval = retval + arg.toByteArray()
        return retval

    def toSignedMessage(self, hashMethod, privateKey):
        message = self.toByteArray()
        envelope = LiveMessage(LiveMessage.signatureForMessage(message, hashMethod, privateKey))
        envelope.append(message)
        return envelope.toByteArray()

    def verifySignature(self, hashMethod, privateKey):
        signature = self.name()
        rawMessage = self.argument(0).stringVal
        return (self.signatureForMessage(rawMessage, hashMethod, privateKey) == signature)

    @staticmethod
    def fromByteArray(rawString):
        list = []
        start = 0
        while (start < len(rawString)):
            start, token = LiveMessageToken.parseToken(rawString, start)
            if (token.valueType == LiveMessageToken.TYPE_INVALID):
                break
            list.append(token)

        msg = LiveMessage()
        msg.args = list
        return msg

    @staticmethod
    def signatureForMessage(msg, hashMethod, privateKey):
        h = 0
        if (hashMethod == "sha512"):
            h = hashlib.sha512()
        elif (hashMethod == "sha256"):
            h = hashlib.sha256()
        else:
            h = hashlib.sha1()

        h.update(msg)
        h.update(privateKey)
        return h.hexdigest().lower()


class ServerList():
    def __init__(self):
        self.list = []
        self.retrieveServerList()

    def popServer(self):
        if (self.list == []):
            self.retrieveServerList()

        if (self.list == []):
            return False

        return self.list.pop(0)


    def retrieveServerList(self):
        conn = httplib.HTTPConnection("api.telldus.com:80")
        conn.request('GET', "/server/assign?protocolVersion=2")
        response = conn.getresponse()

        p = xml.parsers.expat.ParserCreate()

        p.StartElementHandler = self._startElement
        p.Parse(response.read())

    def _startElement(self, name, attrs):
        if (name == 'server'):
            self.list.append(attrs)

def getDeviceList():
    retlist = []
    devList = Device.objects.filter(hidden = False).order_by('order')
    for dev in devList:
        method = 0
        if dev.onOff:
            method |= 0x3
        if dev.dim:
            method |= 0x10
        if dev.activate:
            method |= 0x40

        state = 2
        stateValue = '0'

        key = dev.getDeviceKey()

        if debug:
            print key

        if key != 'DV_NA':
            try:
                c = CurrentValue.objects.get(key = key)
                
                if c.value.startswith('ON'):
                    state = core.td.TELLSTICK_TURNON
                elif c.value.startswith('OFF'):
                    state = core.td.TELLSTICK_TURNOFF
                elif c.value.startswith('DIM'):
                    state = core.td.TELLSTICK_DIM
                    stateValue = c.value.split(':')[1].strip()                
                else:
                    print 'Unknown state'

            except:
                pass


        retlist.append({'methods': method,
                        'state': state,
                        'stateValue': stateValue,
                        'id': int(dev.id),
                        'name': dev.name.encode('utf-8', 'ignore')})

    if debug:
        print retlist

    return retlist


def handleCommand(args):
    global localSettings
    if debug:
        print 'handleCommand', args
    if (args['action'].stringVal == 'turnon'):
        dev = Device.objects.get(id = args['id'].intVal)
        dev.execute(Command.objects.get(id = 101))
        
    elif (args['action'].stringVal == 'turnoff'):
        dev = Device.objects.get(id = args['id'].intVal)
        dev.execute(Command.objects.get(id = 102))

    elif (args['action'].stringVal == 'dim'):
        dev = Device.objects.get(id = args['id'].intVal)
        dev.execute(Command(id = args['value'].intVal, cmd = 'DIM', argInt = args['value'].intVal))

    else:
        print 'Tellduslive, Unknow command', args
        return

    if ('ACK' in args):
        #Respond to ack
        msg = LiveMessage("ACK")
        msg.append(args['ACK'].intVal)
        localSettings['socket'].write(signedMessage(msg))

def handleMessage(message):
    global currentSettings
    global localSettings

    if debug:
        print 'handleMessage', message.name()
    if (message.name() == "notregistered"):
        params = message.argument(0).dictVal

        currentSettings['UUID'] = params['uuid'].stringVal
        currentSettings['Activation URL'] = params['url'].stringVal
        currentSettings['Enabled'] = False

        updateSetting(PLUGIN_NAME, 'UUID', params['uuid'].stringVal)
        updateSetting(PLUGIN_NAME, 'Activation URL', params['url'].stringVal)
        updateSetting(PLUGIN_NAME, 'Enabled', False)
        try:
            localSettings['socket'].close()
            del localSettings['socket']
        except:
            pass
        
        return

    if (message.name() == "registered"):
        params = message.argument(0).dictVal
        localSettings['supportedMethods'] = params['supportedMethods'].intVal
        sendDevicesReport()
        return
    
    if (message.name() == "command"):
        handleCommand(message.argument(0).dictVal)
        return
    
    if (message.name() == "pong"):
        return

    if (message.name() == "disconnect"):
        print 'Ordered to disconnect'
        raise RuntimeError

    print "Did not understand: %s" % message.toByteArray()


def sendDevicesReport():
    global localSettings
    msg = LiveMessage("DevicesReport")
    msg.append(getDeviceList())
    try:
        localSettings['socket'].write(signedMessage(msg))
    except:
        if debug:
            print 'Error sending to socket'        

def sendDeviceEvent(deviceId, method, data):
    global localSettings
    if debug:
        print 'sendDeviceEvent', repr(deviceId), repr(method), repr(data)
    msg = LiveMessage("DeviceEvent")
    msg.append(int(deviceId))
    msg.append(int(method))
    msg.append(str(data))
    try:
        localSettings['socket'].write(signedMessage(msg))
    except:
        if debug:
            print 'Error sending to socket'

def signedMessage(message):
    global currentSettings
    global localSettings

    if debug:
        print localSettings['hashMethod']
        print localSettings['Private key']

    return message.toSignedMessage(localSettings['hashMethod'], localSettings['Private key'])


def signalHandler(signal):
    global currentSettings
    if not currentSettings or signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        print 'Configuration changed tellduslive'
        currentSettings = getSettings(PLUGIN_NAME)
        workQueue.put('settings')
        try:
            localSettings['socket'].close()
            del localSettings['socket']
        except:
            pass

    if signal.content == 'terminate':
        workQueue.put('terminate')
        try:
            localSettings['socket'].close()
            del localSettings['socket']
        except:
            pass

    elif signal.content.startswith('tellduslive,reset'):
        workQueue.put('reset')
        try:
            localSettings['socket'].close()
            del localSettings['socket']
        except:
            pass

    elif currentSettings['Enabled'] and signal.content.startswith('currentvalue_changed'):
        if signal.content.split(',')[1].startswith('DV'):
            if debug:
                print 'Got signal', signal.content
            _t = signal.content.split(':')
            deviceId = int(_t[0].split(',')[1][2:])
            if _t[1] == 'ON':
                method = core.td.TELLSTICK_TURNON
                data = 0
            elif _t[1] == 'OFF':
                method = core.td.TELLSTICK_TURNOFF
                data = 0
            elif _t[1] == 'DIM':
                method = core.td.TELLSTICK_DIM
                data = str(int(int(_t[2])*2.55))
                
            sendDeviceEvent(deviceId, method, data)

        
def threadFunc():
    serverList = ServerList()
    pongTimer = 0
    pingTimer = 0

    global currentSettings
    global localSettings

    server = False

    while(1):
        try:
            s = workQueue.get(False)
            workQueue.task_done()
            if s == 'terminate':
                print 'Time to terminate for tellduslive plugin'
                return

        except:
            pass
        
        currentSettings = getSettings('tellduslive')

        if debug:
            print 'Current settings', currentSettings

        if not currentSettings['Enabled']:
            print 'Not enabled wait'
            try:
                s = workQueue.get(True, 20)
                workQueue.task_done()
                if s == 'terminate':
                    print 'Time to terminate for tellduslive plugin'
                    return
            except:
                pass
            
            continue

        

        if debug:
            print 'Picking a new telldus live server'
        server = serverList.popServer()

        while server == False:
            if debug:
                print 'Cant find any server to connect to, wait a while and try agina later.'
            try:
                s = workQueue.get(True, 60*3)
                workQueue.task_done()
                if s == 'terminate':
                    print 'Time to terminate for tellduslive plugin'
                    return
            except:
                pass
                
            serverList = ServerList()
            server = serverList.popServer()

        startSession(server)

def startSession(server):
    global currentSettings
    global localSettings

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    localSettings['socket'] = ssl.wrap_socket(s, ssl_version=ssl.PROTOCOL_TLSv1, ca_certs="/etc/ssl/certs/ca-certificates.crt",cert_reqs=ssl.CERT_REQUIRED)
    localSettings['socket'].settimeout(5)
    localSettings['socket'].connect((server['address'], int(server['port'])))

    if debug:
        print 'Server connect done *********************'

    uuid = ''
    try:
        uuid = currentSettings['UUID']
    except:
        pass

    if uuid.strip() == '':
        uuid = ''

    msg = LiveMessage('Register')
    msg.append({
            'key': localSettings['Public key'],
            'uuid': currentSettings['UUID'],
            'hash': localSettings['hashMethod']
            })
    msg.append({
            'protocol': 2,
            'version': '1',
            'os': 'linux',
            'os-version': 'unknown'
            })

    localSettings['socket'].write(signedMessage(msg))
    pongTimer = time.time()
    pingTimer = time.time()
    
    print 'Logged in to Tellduslive'

    while(1):
        try:
            resp = localSettings['socket'].read(1024)

        except ssl.SSLError:
            # Timeout, try again after some maintenance
            if (time.time() - pongTimer >= 360):  # No pong received
                print("No pong received, disconnecting")                
                localSettings['socket'].close()
                localSettings['socket'] = None
                break
            if (time.time() - pingTimer >= 120):
                # Time to ping
                msg = LiveMessage("Ping")
                localSettings['socket'].write(signedMessage(msg))
                pingTimer = time.time()

            continue
        except (KeyError, AttributeError):
            return

        if (resp == ''):
            print("no response")
            break

        envelope = LiveMessage.fromByteArray(resp)
        if (not envelope.verifySignature(localSettings['hashMethod'], localSettings['Private key'])):
            print "Signature failed"
            continue

        pongTimer = time.time()        
        try:
            handleMessage(LiveMessage.fromByteArray(envelope.argument(0).stringVal))
        except:
            print 'Error in handleMessage in ' + PLUGIN_NAME
            strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            for s in strings:
                print s

            return
        

def init():
    settings = {'UUID': ('string', ''),
                'Enabled': ('boolean', False),
                'Activation URL': ('string', '')}

    return ([PLUGIN_NAME, 'currentvalue_changed'], settings, signalHandler, threadFunc)
