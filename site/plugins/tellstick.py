from django.db import models
import core.td
import signals.models
from core.models import RawTellstickDevice, CurrentValue
import time
import Queue
import traceback
import sys

PLUGIN_NAME = 'tellstick'

debug = False
init_done = False
raw_enabled = False
callbacks = {}

def sensorEvent(protocol, model, id, dataType, value, timestamp, callbackId):
    if debug:
        print 'sensorEvent happend'

    if init_done:
        signals.models.postToQueue('tellstick,sensor,protocol:' + protocol + ',id:' + str(id) + ',' + core.td.sensorValueTypeReadable[dataType] + ',value:' + str(value), PLUGIN_NAME)

def deviceEvent(deviceId, method, data, callbackId):
    if debug:
        print 'deviceEvent happend'

    if init_done:
        signals.models.postToQueue('tellstick,device:' + core.td.getName(deviceId) + ',id:' + str(deviceId) + ',method:' + core.td.methodsReadable[method] + ',data:' + str(data), PLUGIN_NAME)
        try:
            try:
                dev = RawTellstickDevice.objects.get(deviceId = deviceId)
            except:
                print 'Cant find tellstick device for deviceId = ', deviceId

            key = dev.getKey()
            print 'Key for device is', key
        
            try:
                cv = CurrentValue.objects.get(key = key)
                cv.value = core.td.methodsReadable[method] + ':' + str(int(int(data)/2.55))
            except:
                cv = CurrentValue(key = key, value = core.td.methodsReadable[method] + ':' + str(int(int(data)/2.55)), dataType = 0)

            cv.save()
            print 'CurrentValue updated'
        except:
            strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            for s in strings:
                print s
            

def rawDeviceEvent(data, controllerId, callbackId):
    if debug:
        print 'RawDeviceEvent', data

    if init_done:
        signals.models.postToQueue('tellstick,raw:' + data, PLUGIN_NAME)


workQueue = Queue.Queue()

def signalHandler(signal):
    if signal.content == 'terminate':
        workQueue.put(None)        

    elif signal.content.startswith('tellstick,do:'):
        for i in signal.content[13:].split(';'):
            if i != '':
                workQueue.put(i)

def devLaterInQueue(q, dev):
    for item in q:
        if item[0] == dev:
            return True
    return False
        
def threadFunc():
    keepRunning = True
    while(keepRunning):
        try:
            s = workQueue.get()            

            if s == None:
                print 'Got a none from tellstick workQueue'
                workQueue.task_done()
                keepRunning = False
                continue

            toDo = []
            while s:
                toDo.append(s.split(','))
                workQueue.task_done()
                try:
                    s = workQueue.get(False)
                except:
                    s = None
            
            while len(toDo) > 0:
                s = toDo.pop(0)
                
                if not devLaterInQueue(toDo, s[0]):
                    if debug:
                        print 'Time to execute tellstick command', s

                    cmd = s[1].lower()
                    for i in range(int(s[3])):
                        if cmd == 'on':
                            core.td.turnOn(int(s[0]))
                        elif cmd == 'off':
                            core.td.turnOff(int(s[0]))
                        elif cmd == 'bell':
                            core.td.bell(int(s[0]))
                        elif cmd == 'dim':
                            core.td.dim(int(s[0]), int(int(s[2])*2.55))
                        elif cmd == 'up':
                            core.td.up(int(s[0]))
                        elif cmd == 'down':
                            core.td.down(int(s[0]))
                        elif cmd == 'stop':
                            core.td.stop(int(s[0]))
                        elif cmd == 'learn':
                            core.td.learn(int(s[0]))
                        else:
                            print 'Unknown command', s

        except:
            print 'Error executing tellstick command'
            if debug:
                raise
    


def init():
    global init_done
    init_done = True

    core.td.init()

    callbacks['sensor'] = core.td.registerSensorEvent(sensorEvent)
    callbacks['device'] = core.td.registerDeviceEvent(deviceEvent)
    callbacks['raw'] = core.td.registerRawDeviceEvent(rawDeviceEvent)

    if debug:
        print 'registerSensorEvent', callbacks['sensor']
        print 'registerDeviceEvent', callbacks['device']
        print 'registerRawDeviceEvent', callbacks['raw']

    settings = {}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)

