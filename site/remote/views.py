from django.http import HttpResponse
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.views.decorators.cache import cache_control
from remote.models import getPreRendered, Remote

def remote(request, remote_id = None, page = 0):
    if remote_id == None:
        remote_id = request.session.get('remote_id', None)
        
    if remote_id == None:
        return redirect('/remote/change/')
    else:
        request.session.set_expiry(60*60*24)

    return getPreRendered(remote_id, page)

def change(request, remote_id = None):
    if remote_id != None:
        request.session['remote_id'] = remote_id
        request.session.set_expiry(60*60*24) #24 hours
        return redirect('/remote/')

    remotes = Remote.objects.order_by('name')
    try:
        myRemote = int(request.session.get('remote_id', -1))
        r = Remote.objects.get(pk = myRemote)
    except:
        myRemote = -1

    return render(request, 'admin/remote_change.html', {'remotes': remotes, 'myRemote': myRemote})
