from remote.models import Theme, Remote, Widget, Page, SingleDevCmd, OnOffDev, DimDev, VariableValue, Link, Heading, GenericContent

from django.contrib import admin

class PageAdmin(admin.ModelAdmin):
    list_disply = ('remote', '__unicode__')

class WidgetAdmin(admin.ModelAdmin):
    ordering = ('page', 'y', 'x')
    list_display = ('__unicode__', 'page', 'y', 'x')

#admin.site.register(Theme)
admin.site.register(Remote)
#admin.site.register(Widget, WidgetAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(SingleDevCmd)
admin.site.register(DimDev)
admin.site.register(OnOffDev)
admin.site.register(VariableValue)
admin.site.register(Link)
admin.site.register(Heading)
admin.site.register(GenericContent)


