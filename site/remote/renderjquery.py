from django.http import Http404, HttpResponse

import models

debug = False

def render(remote, baseUrl, staticBaseUrl):
    cfg = models.getConfig(remote)

    pages = []
    widgets = models.Widget.objects.filter(remote = remote)
    for w in widgets:
        if w.page not in pages:
            pages.append(w.page)
    
    maxpage = len(pages) - 1

    ret = {}
    for p in range(maxpage + 1):
        ret[p] = HttpResponse(renderpage(remote, pages[p], p, maxpage, baseUrl, staticBaseUrl))

    return ret


def renderpage(remote, page, pagenr, maxpage, baseUrl, staticBaseUrl):
    leftpage = pagenr - 1
    if leftpage < 0:
        leftpage = maxpage

    rightpage = pagenr + 1
    if rightpage > maxpage:
        rightpage = 0

    if debug:
        print 'leftpage:' + baseUrl + str(leftpage) + '/'
        print 'rightpage:' + baseUrl + str(rightpage) + '/'
    
    s = u"""<!DOCTYPE html> 
<html> 
<head> 
<title>"""
    s += page.name

    s += """ - Automagically</title> 
<meta name="viewport" content="width=device-width, initial-scale=1"> """
    if remote.style == '':
        s += '<link rel="stylesheet" href="%sjquery/jquery.mobile-1.2.0.min.css" />' %(staticBaseUrl)
    else:
        s += '<style type="text/css">\n' + remote.style + '\n</style>\n<link rel="stylesheet" href="%sjquery/jquery.mobile.structure-1.2.0.min.css" />\n' %(staticBaseUrl)
        
    s += '<script src="%sjquery/jquery-1.8.2.min.js"></script><script src="%sjquery/jquery.mobile-1.2.0.min.js"></script>' %(staticBaseUrl, staticBaseUrl)
        

    s += """
<style type="text/css">
input.ui-slider-input {
    display:none !important;
}
input.slider {
    data-highlight:true;
}
</style>
<script>
  var dev_timer = null;
  var gv_timer = null;
  var pageInitialized = false;
</script>
</head> 
<body> 
<script>
  function devcmd(dev,cmd) {
    //alert("devcmd called " + dev + " " + cmd);
    $.post("/rest/do/", {type: "COMMAND", commandid: cmd, deviceid: dev}, function (response) {
       if(response.success){
         if(response.message){
         } else {
         }
       } else {
         if(response.message){
         } else {
         }
      }
    }, 'json');
    //alert("updateDeviceStateDo()");
    
    cmdTxt = commandIdToCmd(cmd);
    if(cmdTxt) {
      updateSingleDeviceState(cmdTxt, "DV" + dev);
    }
    //setTimeout(updateDeviceState, 100);
  }

  function commandIdToCmd(cmdid) {
    var cmd = null;
    if(cmdid == 101) {
      return "ON:0";
    } else if(cmdid == 102) {
      return "OFF:0";
    } else if(cmdid >= 0 && cmdid <= 100) {
      return "DIM:" + cmdid;
    }
    return null;
  }


  function updateVariables() {
    $.getJSON("/rest/gv/all/", function(data) {
      var oldstr = "";
      if(data.success == true){
        $.each(data, function(key, val) {
          if(key != "success"){
            if(val.old){
              oldstr = " missing updates";
            }
            if(val.unit == ""){
              $("#"+val.key).html(val.value + oldstr);
            } else {
              $("#"+val.key).html(val.value + "&nbsp;" + val.unit + oldstr);
            }
          }
        });
      }
    });
    gv_timer=setTimeout(updateVariables, 10000);
  }

  function updateSingleDeviceState(cmd, dev) {
    if(cmd.substr(0,2) == "ON"){
      $("div[key='" + dev + "']").html("ON");
      $("input.devdim[key='" + dev + "']").val(100);
      $("input.devdim[key='" + dev + "']").slider('refresh');
    } else if(cmd.substr(0,3) == "OFF"){
      $("div[key='" + dev + "']").html("OFF");
      $("input.devdim[key='" + dev + "']").val(0);
      $("input.devdim[key='" + dev + "']").slider('refresh');
    } else if(cmd.substr(0,3) == "DIM"){
      $("div[key='" + dev + "']").html(cmd);
      $("input.devdim[key='" + dev + "']").val(cmd.substr(4));
      $("input.devdim[key='" + dev + "']").slider('refresh');
    } else {
      alert("Unsupported device state" + cmd);
    }
  }

  function updateDeviceStateDo() {
    $.getJSON("/rest/dev/all/", function(data) {
      if(data.success == true){
        $.each(data, function(key, status) {
          if(key != "success"){
            updateSingleDeviceState(status.value, status.key);
          }
        });
      }
    });
  }
  function updateDeviceState(){
    if(dev_timer) {
      clearTimeout(dev_timer)
    }
    updateDeviceStateDo();
    dev_timer=setTimeout(updateDeviceState, 10000);
  }

  function changePage(url){
    if(dev_timer!=null){
      //clearTimeout(dev_timer);
    }
    if(gv_timer!=null){
      //clearTimeout(gv_timer);
    }

    $.mobile.changePage(url, { transition: "pop"} );
  }

  function leadingzero(num) {
    var s = "0" + num;
    return s.substr(s.length-2);
  }

  function opencreateeventform(deviceid, onoff, activate, dim) {
    var d = new Date();

    $("#event-redirect").val(document.location.href);
    if(deviceid) {
    }

    var dstr = d.getFullYear() + "-" + leadingzero(d.getMonth() + 1) + "-" + leadingzero(d.getDate());
    $("#dodate").val(dstr);
    $("#dotime").val(d.toLocaleTimeString());

    updateeventselectcommand($("#event-dev"));

    $("#evcreatepage").popup("open");
  }

  function updateeventselectcommand(t) {
    var s = $(t).val().split(":");
    //var s = $("#event-command").val().split(":");
    var optionhtml = "";
    if(s[1] == "1") {
      optionhtml += '<option value="101">ON</option>';
      optionhtml += '<option value="102">OFF</option>';
    }
    if(s[2] == "1") {
      optionhtml += '<option value="105">ACTIVATE</option>';
    }
    if(s[3] == "1") {
      optionhtml += '<option value="10">DIM 10</option>';
      optionhtml += '<option value="20">DIM 20</option>';
      optionhtml += '<option value="30">DIM 30</option>';
      optionhtml += '<option value="40">DIM 40</option>';
      optionhtml += '<option value="50">DIM 50</option>';
      optionhtml += '<option value="60">DIM 60</option>';
      optionhtml += '<option value="70">DIM 70</option>';
      optionhtml += '<option value="80">DIM 80</option>';
      optionhtml += '<option value="90">DIM 90</option>';
      optionhtml += '<option value="100">DIM 100</option>';
    }
    $("#event-command").html(optionhtml).selectmenu('refresh', true);
  }

  function createevent(deviceid, commandid, time, date){
    $.post("/rest/do/", {type: "CREATEEVENT", deviceid: deviceid, commandid: commandid, dotime: time, dodate: date}, function (response) {
       if(response.success){
         if(response.message){
         } else {
         }
         $("#evcreatepage").popup("close");
       } else {
         if(response.message){
           alert(response.message)
         } else {
           alert("Unknown error creating event, please report this as bug.");
         }
      }
    }, 'json');
  }


  function cancelevent(eventid){
    $.post("/rest/do/", {type: "CANCELEVENT", eventid: eventid}, function (response) {
       if(response.success){
         if(response.message){
         } else {
         }
       } else {
         if(response.message){
         } else {
         }
      }
    }, 'json');
  }
  function delayevent(eventid, minutes){
    $.post("/rest/do/", {type: "DELAYEVENT", eventid: eventid, time: minutes}, function (response) {
       if(response.success){
         if(response.message){
         } else {
         }
       } else {
         if(response.message){
         } else {
         }
      }
    }, 'json');
  }

</script>

<div data-role="page" data-theme="a" data-content-theme="a">
<div data-role="header" data-position="fixed">"""
    s += '<a href="#" onClick="changePage(\'%s\')" data-icon="arrow-l" data-iconpos="notext"></a>' %(baseUrl + str(leftpage) + '/')
    s += '<h1>' + page.name + '</h1>'
    s += '<a href="#" onClick="changePage(\'%s\')" data-icon="arrow-r" data-iconpos="notext"></a>' %(baseUrl + str(rightpage) + '/')

    s += """
<div data-role="navbar">
<ul>
<li><a href="#evpage" data-rel="popup" data-position-to="window">Events</a></li>
<li><a href="#" id="navbaropenschedule">Schedule</a></li>
<li><a href="/admin/" rel="external">Admin</a></li>
</ul>
</div><!-- /navbar -->
</div><!-- /header -->

<div data-role="content">
   <script>
     $(document).ready(function(event) {

       if(pageInitialized){
         return;
       }
       pageInitialized = true;

       $("a.devcmd").click(function(event){
         devcmd($(this).attr('dev'),$(this).attr('cmd'));
         event.preventDefault();
       });
       $("input.devdim").on("slidestart", function( event ){
         if($(this).attr('dev') != "undefined") {
           devcmd($(this).attr('dev'),$(this).attr('value'));
         }
       });
       $("a.statpopup").bind('click', function(event){
         if($(this).attr('stat-unit') == ''){
           $("#stat-text").html($(this).attr('stat-text') + ' in last 24h<br>');
         } else {
           $("#stat-text").html($(this).attr('stat-text') + ' in last 24h<br>in ' + $(this).attr('stat-unit') + '<br>');
         }
         $("#stat-img").attr('src', '/stats/24h/GV' + $(this).attr('stat-var') + '.png'); 
       });

       $("#createeventsubmit").click(function(event) {
         createevent($("#event-dev").val().split(":")[0], $("#event-command").val(), $("#dotime").val(), $("#dodate").val());
       });

       $("#navbaropenschedule").click(function(event){
         opencreateeventform(null, true, true, true);

       });

       $('#evcreatepage').bind('popupafterclose', function(event, ui) {
         $("#navbaropenschedule").removeClass('ui-btn-active');
       });

       $("#stat-GV").bind('popupafterclose', function(event, ui){
         $("#stat-img").attr('src', '/static/automagically/noplot.png'); 
       });

       $("#evpage").bind('popupbeforeposition', function(event){
         $.getJSON("/rest/events/", function(data) {
           var evlist = '';
           if(data.success == true){
             $.each(data.events, function(key, val) {
               evlist += '<div data-role="collapsible" data-inset="false"><h3>';
               evlist += val.device
               evlist += ' ' + val.command + '<br>'
               evlist += val.doin + '</h3><p>' + val.doat + '</p>'
               evlist += '<ul class="eventul" data-role="listview">';
               evlist += '<li><a href="#" class="eventcancel" eventid="' + val.eventid + '">Cancel</a></li>'
               evlist += '<li><a href="#" class="eventdelay1h" eventid="' + val.eventid + '">Delay 1h</a></li>'
               evlist += '</ul></div>'
             });
           }
           //alert(evlist);
           $("#events").html(evlist);
           
           $("#events").find('div[data-role=collapsible]').collapsible({theme:'a',refresh:true});
           $("ul.eventul").listview({refresh: true});
           $("a.eventcancel").click(function(event){
             cancelevent($(this).attr('eventid'));
             $("#evpage").popup("close")
             event.preventDefault();
           });
           $("a.eventdelay1h").click(function(event){
             delayevent($(this).attr('eventid'), 60);
             $("#evpage").popup("close")
             event.preventDefault();
           });
         });         
      });

      $("#event-dev").on( "change", function(event, ui) {
        updateeventselectcommand(this);
      });

      $(document).bind("swipeleft",function(event) {
        // alert("Swipeleft");
"""

    s += 'changePage("%s");' %(baseUrl + str(leftpage) + '/')

    s += """
       });
       $(document).bind("swiperight",function(event) {
         //alert("Swiperight");
"""
 
    s += 'changePage("%s");' %(baseUrl + str(rightpage) + '/')

    s += """
       });

       //alert("Settimeout called");
       if(gv_timer==null){
         gv_timer = setTimeout(updateVariables, 10);
       }
       if(dev_timer==null){
         dev_timer = setTimeout(updateDeviceState, 10);
       }
"""
    for p in range(maxpage + 1):
        if p != pagenr:
            s += '      $.mobile.loadPage("%s%s/");\n' %(baseUrl, p)

    s += """
     });

     $.event.special.swipe.durationThreshold = 400;
     $.event.special.swipe.verticalDistanceThreshold = 30;
     $.event.special.swipe.horizontalDistanceThreshold = 150;
   </script>
"""


    widgets = models.Widget.objects.filter(remote = remote, page = page)
    
    currentY = None
    currentX = None
    xValues = None
    
    for w in widgets.order_by('y', 'x'):
        if debug:
            print 'Widget', w, w.y, w.x, currentY, currentX, xValues

        if currentY != w.y and currentY != None:
            if debug:
                print 'New Y detected'
            s += '</div><!-- closing x -->\n'
            s += '</div><!-- closing y -->\n'
            currentX = -1

        elif currentX != w.x and currentX != None:
            if debug:
                print 'New X detected'
            s += '</div><!-- closing x -->\n'            
            

        if currentY != w.y:
            #New line code should be rendered here
            #Count number of distinct X
            xValues = {}
            for wt in widgets.filter(y = w.y):
                xValues[wt.x] = 1
            if debug:
                print xValues
                print 'Total Xvalues for this line is', len(xValues)

            if len(xValues) == 1:
                gridClass = 'ui-grid-solo'
            elif len(xValues) == 2:
                gridClass = 'ui-grid-a'
            elif len(xValues) == 3:
                gridClass = 'ui-grid-b'
            elif len(xValues) == 4:
                gridClass = 'ui-grid-c'
            elif len(xValues) == 5:
                gridClass = 'ui-grid-d'
            
            s += '<div class="' + gridClass + '">\n'
            xcell = 0

        if currentX != w.x:
            xcell += 1

            if xcell == 1:
                s += '<div class="ui-block-a">'
            elif xcell == 2:
                s += '<div class="ui-block-b">'
            elif xcell == 3:
                s += '<div class="ui-block-c">'
            elif xcell == 4:
                s += '<div class="ui-block-d">'

        

        wi, t = w.getSubTypeInstance()
        if t == 'SingleDevCmd':
            s += '<a class="devcmd" dev="' + str(wi.dev.id) + '" cmd="' + str(wi.cmd.id) + '" href="#" data-role="button">' + w.__unicode__() + '</a>\n'

        elif t == 'OnOffDev':
            s += '<div data-role="controlgroup" data-type="horizontal">\n'
            s += '<center>' + w.__unicode__() + '&nbsp;<div class="devtextdiv" key="' + str(wi.dev.getDeviceKey()) + '" style="display:inline">?</div></center>'
            s += '<a class="devcmd" href="#" dev="' + str(wi.dev.id) + '" cmd="102" data-role="button">Off</a>\n'
            s += '<a class="devcmd" href="#" dev="' + str(wi.dev.id) + '" cmd="101" data-role="button">On</a>\n'
            s += '</div>\n'

        elif t == 'OnOffDevEXPERIMENTAL':
            s += '<div data-role="fieldcontain">'
            s += '<center>' + w.__unicode__() + '&nbsp;&nbsp;<div class="devtextdiv" key="' + str(wi.dev.getDeviceKey()) + '" style="display:inline">?</div></center>'
            s += '<fieldset data-role="controlgroup" data-type="horizontal">'
            s += '<input type="checkbox" name="checkbox-6" id="checkbox-6" class="custom" />'
            s += '<label for="checkbox-6">Off</label>'

            s += '<input type="checkbox" name="checkbox-7" id="checkbox-7" class="custom" />'
            s += '<label for="checkbox-7">On</label>'
            s += '</fieldset></div>'

        elif t == 'DimDev':
            s += '<center>' + w.__unicode__() + '</center>'
            s += '<a class="devcmd" dev="' + str(wi.dev.id) + '" cmd="102" href="#" data-role="button" data-inline="true">Off</a>\n'
            s += '<input class="devdim" key="' + str(wi.dev.getDeviceKey()) + '" dev="' + str(wi.dev.id) + '" cmd="DIM" type="range" value="0" min="0" max="100" data-mini="true" data-highlight="true"/>'
        elif t == 'VariableValue':
            if wi.var.dataType != 0 and wi.var.source == '': #String datatype is not possible to show plot of
                s += '<a class="statpopup" stat-unit="' + wi.var.unit + '" href="#stat-GV" stat-var="' + str(wi.var.id) + '" stat-text="' + w.__unicode__() + '" data-rel="popup">'
            s += '<center>'
            if wi.var.source == '':
                s += w.__unicode__() + '<br>'
            else:
                s += '<div class="VariableSource">' + wi.var.source + '</div>'
            s += '<div class="VariableValue" id="GV' + str(wi.var.id) + '">&nbsp;</div>'
            s += '</center>'

            if wi.var.dataType != 0: #String datatype is not possible to show plot of
                s += '</a>'
        elif t == 'GenericContent':
            s += '<div class="genericcontent">\n'
            s += wi.content
            s += '</div>\n'

        elif t == 'Heading':
            s += '<div class="heading">\n'
            if w.displayText != '':
                s += '<h2>' + w.displayText + '</h2>'
            if wi.divider == True:
                s += '<hr>'
            s += '</div>\n'

        elif t == 'Link':
            s += '<div class="link">\n'
            if wi.url != '':
                s += '<a href="' + wi.url + '">' + w.displayText + '</a>'
            s += '</div>\n'


        else:
            s += 'Unsupported widget type'


        currentY = w.y
        currentX = w.x

    if currentY != None:
        s += '</div><!-- /y -->\n'

    s += """</div><!-- /content -->
    <div id="footer">
<form>
</form>
    </div><!-- footer -->
<div data-role="popup" id="evcreatepage">
<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
<h2 id="event-devicename">Schedule</h2>
<form action="/rest/do/" method="POST" data-ajax="false">
<input type="hidden" name="type" value="CREATEEVENT">
<input type="hidden" id="event-redirect" name="redirect" value="">
<select id="event-dev">
"""
    for w in widgets.order_by('y', 'x'):
        wi, t = w.getSubTypeInstance()
        if t in ['SingleDevCmd', 'OnOffDev', 'DimDev']:
            s += '<option value="' + str(wi.dev.id) + ':'
            if wi.dev.onOff:
                s += '1:'
            else:
                s += '0:'
            if wi.dev.activate:
                s += '1:'
            else:
                s += '0:'
            if wi.dev.dim:
                s += '1'
            else:
                s += '0'
            s += '">' + w.__unicode__() + '</option>\n'

    s+= """</select>
<label for="event-command" class="ui-hidden-accessible">Command:</label>
<select name="commandid" id="event-command" data-mini="true">
<option value="101">ON</option>
</select>
<input type="time" id="dotime" name="dotime">
<input type="date" id="dodate" name="dodate">
<center>
<a href="#" id="createeventsubmit" data-role="button">Create</a>
</center>
</form>
<center><a href="#" data-rel="back" data-role="button">Close</a></center>
</div>
<div data-role="popup" id="evpage">
<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
<h2>Events</h2>
<div data-role="collapsible-set" data-theme="a" data-content-theme="c" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" style="margin:0; width:250px;">
<div id="events">
</div>
</div>
<br>
<center><a href="#" data-rel="back" data-role="button">Close</a></center>
</div>
    </div><!-- /page -->
<div data-role="popup" id="stat-GV">
<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
<center><div id="stat-text">&nbsp;</div>
<a href="#" data-rel="back"><img id="stat-img" src="/static/automagically/noplot.png" alt="Matplotlib not installed, unable to make plot" width=300 height=225></a></center>
</div>
</body>
</html>"""

    return s

