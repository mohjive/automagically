from django.db import models, connection
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError

debug = False

import os
import sys
import time
import datetime
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST
import urllib2
import traceback
import xml.etree.ElementTree as ET

from signals.scanf import scanf_compile
import re

from pytz import timezone
try:
    import ephem
    EPHEM_AVAILABLE = True
except:
    EPHEM_AVAILABLE = False

import td
import ctypes

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins')

sys.path.append(PLUGINS_PATH)

from general import getSetting

class Command(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    cmd = models.CharField(max_length=15)
    argInt = models.PositiveIntegerField(default = 0)

    def __unicode__(self):
        if self.id > 100:
            return self.cmd
        else:
            return self.cmd + ' ' + str(self.argInt)
        
    def getExecCmd(self, deviceId):
        if self.cmd == 'ACTIVATE':
            print 'Should never reach getExecCmd for cmd type ACTIVATE'
            return ''
        elif self.cmd == 'DIM':
            return '--dimlevel ' + str(int(2.55*self.argInt)) + ' --dim ' + str(deviceId)
        else:
            return '--' + self.cmd.lower() + ' ' + str(deviceId)

def validateInt(f, max, min, protocol, field):
    try:
        x = int(f)
    except:
        raise ValidationError('For %s %s should be an integer %d-%d' %(protocol, field, min, max))
    if x < min or x > max:
        raise ValidationError('For %s %s should be an integer %d-%d' %(protocol, field, min, max))

def validateCharIn(f, allowed, nrChars, protocol, field):
    if len(f) != nrChars:
        raise ValidationError('For %s %s should be %d characters' %(protocol, field, nrChars))
    for c in f:
        if c not in allowed:
            raise ValidationError('For %s %s should only be characters from %s' %(protocol, field, allowed))
                              
    
def validateEmpty(f, protocol, field):
    if f != '':
        raise ValidationError('For %s %s should be empty' %(protocol, field))
            


class Device(models.Model):
    name = models.CharField(max_length=40, unique = True, help_text = "Name of the device, is used to refer to it in various places. Can contain unicode chars.")
    order = models.PositiveIntegerField(default = 100, help_text = "In default view all devices is sorted by this value and arranged low to high")
     
    activate = models.BooleanField()
    onOff = models.BooleanField()
    dim = models.BooleanField()
    hidden = models.BooleanField(help_text = "If ticked this is not visible on the default view")
    
    def __unicode__(self):
        return self.name

    def getDeviceKey(self):
        if hasattr(self, 'rawtellstickdevice'):
            return self.rawtellstickdevice.getKey()
        return 'DV_NA'


    def containDevice(self, devicesNotAllowed):
        print 'ContainDevice called for ', repr(self)
        devicesNotAllowed.append(self)
        
        if hasattr(self, 'rawtellstickdevice'):
            return self.rawtellstickdevice.containRawDevice(devicesNotAllowed)
        elif hasattr(self, 'groupdevice'):
            return self.groupdevice.containGroupDevice(devicesNotAllowed)
        elif hasattr(self, 'timerdevice'):
            return self.timerdevice.containTimerDevice(devicesNotAllowed)
        elif hasattr(self, 'preset'):
            return self.preset.containPresetDevice(devicesNotAllowed)
        else:
            print 'unknown instance type in containDevice', self
        return False
        
    def execute(self, cmd):

        if cmd.cmd == 'ON' and not self.onOff:
            return False
        elif cmd.cmd == 'OFF' and not self.onOff:
            return False
        elif cmd.cmd == 'DIM' and not self.dim:
            return False
        elif cmd.cmd == 'ACTIVATE' and not self.activate:
            return False
            
        if hasattr(self, 'rawtellstickdevice'):
            return self.rawtellstickdevice.doExecute(cmd)
        elif hasattr(self, 'sendsignaldevice'):
            return self.sendsignaldevice.doExecute(cmd)
        elif hasattr(self, 'groupdevice'):
            return self.groupdevice.doExecute(cmd)
        elif hasattr(self, 'timerdevice'):
            return self.timerdevice.doExecute(cmd)
        elif hasattr(self, 'preset'):
            return self.preset.doExecute(cmd)
        elif hasattr(self, 'woldevice'):
            return self.woldevice.doExecute(cmd)
        elif hasattr(self, 'variabledelayedcommand'):
            return self.variabledelayedcommand.doExecute(cmd)

        else:
            print 'unknown instance type'
        return False
    

class RawTellstickDevice(Device):
    PROTOCOLCHOICES = (
        ('arctech', (
                ('arctech;codeswitch', 'arctech codeswitch'),
                ('arctech;bell', 'arctech bell'),
                ('arctech;selflearning-switch', 'arctech selflearning-switch'),
                ('arctech;selflearning-dimmer', 'arctech selflearning-dimmer'))),
        ('risingsun', (
                ('risingsun;codeswitch', 'risingsun codeswitch'),
                ('risingsun;selflearning', 'risingsun selflearning'))),
        ('silvanchip', (
                ('silvanchip;ecosavers', 'silvanchip ecosavers'),
                ('silvanchip;kp100', 'silvanchip kp100'))),
        ('others', (
                ('brateck;', 'brateck'),
                ('everflourish;selflearning', 'everflourish'),
                ('fuhaote;', 'fuhaote'),
                ('hasta;', 'hasta'),
                ('sartano;codeswitch', 'sartano'),
                ('upm;', 'upm'),
                ('waveman;codeswitch', 'waveman codeswitch'),
                ('x10;', 'x10'),
                ('yidong;', 'yidong'))))                  
         
    deviceId = models.PositiveIntegerField(default = 9999)

    rawName = models.CharField(max_length=100, default = '', verbose_name = 'Name in tellstick config', help_text = "A name that is free from special characters and space is recommended")
    protocol = models.CharField(max_length=40, default = '', choices=PROTOCOLCHOICES, help_text = 'In general chose one of the arctech ones for Nexa/Proove/Rusta devices. For full documentation on this field and the house, unit, code fields below see <a href="http://developer.telldus.se/wiki/TellStick_conf">Telldus documentation</a>.')
    house = models.CharField(max_length=20, default = '', blank=True)
    unit = models.CharField(max_length=20, default = '', blank=True)
    code = models.CharField(max_length=20, default = '', blank=True)
    
    controllingSameDeviceAs= models.ManyToManyField('self', verbose_name = "Controlling same device as", related_name='controlling', null=True, blank=True, default = None, help_text = "If you have two RawTellstickDevice:es that control same actual breaker for example if you have two wallswitches and have learnt the system both codes. Select them here to make the device status updated by both buttons.")

    repeats = models.PositiveSmallIntegerField(default = 1, help_text = "Number of times each command should be sent. 1 should be fine for almost all cases.") 

    def containRawDevice(self, device):
        print 'ContainRawDevice for ', repr(self)
        return False

    def clean(self):
        self.activate = False
        self.onOff = True
                
        #Extend this list for device types that support dimming
        if self.protocol in ['arctech;selflearning-dimmer']:
            self.dim = True
        else:
            self.dim = False

        if self.repeats <= 0 or self.repeats > 49:
            raise ValidationError('Repeats needs to be 0 < repeats < 50')

        #Need to make sure house,unit, code is set/unset according to protocol/model
        if self.protocol in ['arctech;codeswitch', 'x10;']:
            validateInt(self.unit, 16, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateCharIn(self.house, 'ABCDEFGHIJKLMNOP', 1, self.protocol, 'house')

        elif self.protocol == 'arctech;bell':
            validateEmpty(self.unit, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateCharIn(self.house, 'ABCDEFGHIJKLMNOP', 1, self.protocol, 'house')

        elif self.protocol in ['arctech;selflearning-dimmer', 'arctech;selflearning-switch']:
            validateInt(self.unit, 16, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 67108863, 1, self.protocol, 'house')
        elif self.protocol == 'brateck;':
            validateEmpty(self.unit, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateCharIn(self.house, '10-', 8, self.protocol, 'house')
        elif self.protocol == 'everflourish;selflearning':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 16383, 0, self.protocol, 'house')
        elif self.protocol in ['fuhaote;', 'sartano;codeswitch']:
            validateEmpty(self.unit, self.protocol, 'unit')
            validateCharIn(self.code, '10', 10, self.protocol, 'code')
            validateEmpty(self.house, self.protocol, 'house')
        elif self.protocol == 'hasta;':
            validateInt(self.unit, 15, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 65536, 1, self.protocol, 'house')
        elif self.protocol == 'risingsun;codeswitch':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 4, 1, self.protocol, 'house')
        elif self.protocol == 'risingsun;selflearning':
            validateInt(self.unit, 16, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 33554432, 1, self.protocol, 'house')
        elif self.protocol == 'silvanchip;ecosavers':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 1048575, 1, self.protocol, 'house')
        elif self.protocol == 'silvanchip;kp100':
            validateEmpty(self.unit, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 1048575, 1, self.protocol, 'house')
        elif self.protocol == 'upm;':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateInt(self.house, 4095, 1, self.protocol, 'house')
        elif self.protocol == 'yidong;':
            validateInt(self.unit, 4, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateEmpty(self.house, self.protocol, 'house')
        elif self.protocol == 'waveman;codeswitch':
            validateInt(self.unit, 16, 1, self.protocol, 'unit')
            validateEmpty(self.code, self.protocol, 'code')
            validateCharIn(self.house, 'ABCDEFGHIJKLMNOP', 1, self.protocol, 'house')
        else:
            raise ValidationError('Protocol: %s not supported' %(self.protocol))

        
        if self.deviceId != 9999:
            removeResult = td.removeDevice(int(self.deviceId))
            if debug:
                print 'Remove device', repr(self.deviceId), removeResult

        else:
            if debug:
                print 'No remove necessary', self.deviceId

        devId = td.addDevice()
        if devId >= 0:
            self.deviceId = devId
        else:
            raise ValidationError('Unable to addDevice to tellstick configuration. Got the following error:' + td.getErrorString(devId))
                
        td.setName(devId, str(self.rawName))
        prot = str(self.protocol)
        td.setProtocol(devId, prot.split(';')[0])
        if len(self.protocol.split(';')) == 2:
            td.setModel(devId, prot.split(';')[1])
        
        if self.house != '':
            td.setDeviceParameter(devId, 'house', str(self.house))
        if self.code != '':
            td.setDeviceParameter(devId, 'code', str(self.code))
        if self.unit != '':
            td.setDeviceParameter(devId, 'unit', str(self.unit))

    def delete(self):
        if debug:
            print 'Remove device on delete'
        td.removeDevice(int(self.deviceId))
        super(RawTellstickDevice, self).delete()
            
    
    def doExecute(self, cmd):
        if debug:
            print 'RawTellstickDevice doExecute', self.deviceId, self.name, cmd

        postToQueue('tellstick,do:' + str(self.deviceId) + ',' + str(cmd.cmd) +',' + str(cmd.argInt) + ',' + str(self.repeats), 'web')

        return True

    def getConf(self):
        s = 'device {\n'
        s += '  id = ' + str(self.deviceId) + '\n'
        s += '  name = "' + self.rawName + '"\n'
        s += '  protocol = "' + self.protocol.split(';')[0] + '"\n'
        if self.protocol.split(';')[1] != '':
            s += '  model = "' + self.protocol.split(';')[1] + '"\n'
        s += '  parameters {\n'
        if self.house != '':
            s += '    house = "' + self.house + '"\n'
        if self.unit != '':
            s += '    unit = "' + self.unit + '"\n'
        if self.code != '':
            s += '    code = "' + self.code + '"\n'
        s += '  }\n'
        s += '}\n'

        return s

    def getKey(self):
        lowestId = self.id

        for related in self.controllingSameDeviceAs.all():
            lowestId = min(lowestId, related.id)

        return 'DV' + str(lowestId)
        
class TimerDevice(Device):
    subDevice = models.ForeignKey(Device, related_name='subdevice', help_text = "Select the device that should be triggered after the timer has elapsed.")
    delayMinutes = models.PositiveIntegerField(default = 1, help_text = "Enter number of minutes for the timer. Only integers allowed.")
    command = models.ForeignKey(Command, default = 102, help_text = "Select command to be executed after the timmer has elapsed.")
    clearOtherEvents = models.BooleanField(default = True, blank = True, help_text= "If selected any already scheduled event/timer device event between the time of activate up untill the delay has elapsed will be removed for the same device. This is genarally wanted behaviour if for example you need to extend the timer, if this is selected then just activate the device again and the old timer elapsed event will be removed.") 
    condition = models.ForeignKey('GlobalVariable', blank = True, null = True, help_text = "When timer has elapsed check this variable and only execute if it is true. True is any number not 0/0.0 or True.")

    def containTimerDevice(self, device):
        return self.subDevice.containDevice(device)

    def save(self):
        self.activate = True
        self.onOff = False
        self.dim = False
        super(TimerDevice, self).save()

    def doExecute(self, cmd):
        if debug:
            print 'TimerDevice doExecute'
        local = timezone(settings.TIME_ZONE)

        if self.clearOtherEvents:
            if debug:
                print 'Time to clear other events'
            evs = Event.objects.filter(device = self.subDevice).filter(doAt__lt = local.localize(datetime.datetime.now()) + datetime.timedelta(minutes = self.delayMinutes))
            for e in evs:
                print '  Event to clear:'
                print '  ', e.device, e.doAt
                e.delete()

        e = Event(device = self.subDevice,
                  command = self.command,
                  doAt = local.localize(datetime.datetime.now()) + datetime.timedelta(minutes = self.delayMinutes),
                  condition = self.condition)
        e.save()



class GroupDevice(Device):
    subDevices = models.ManyToManyField(Device, related_name='subdevices', help_text = "Devices in the group, select how many/few you want, but make sure not to select something that referes back to itself.")

    def containGroupDevice(self, devicesNotAllowed):
        print 'ContainGroupDevice on ', repr(self)

        print 'Not allowed devices', devicesNotAllowed
        for dev in self.subDevices.all():
            print 'Now testing ', repr(dev)
            if dev in devicesNotAllowed:
                return True



            print 'Contain', repr(dev)
            if dev.containDevice(devicesNotAllowed):
                print 'True'
                return True
        return False


    def save(self):
        self.activate = False
        self.onOff = True        
        self.dim = True
        super(GroupDevice, self).save()
        for subdev in self.subDevices.all():
            if not subdev.dim:
                self.dim = False
                super(GroupDevice, self).save()
                break
            
    def doExecute(self, cmd):
        for dev in self.subDevices.all():
            dev.execute(cmd)


class Preset(Device):
    def save(self):
        self.activate = True
        self.onOff = False
        self.dim = False
        super(Preset, self).save()

    def containPresetDevice(self, device):
        for dev in PresetEntry.objects.filter(presetDevice = self):
            if dev.containDevice(device):
                return True
        return False


    def doExecute(self, cmd):
        if debug:
            print 'Preset doExecute'

        for p in PresetEntry.objects.filter(presetDevice = self):
            if debug:
                print p
            p.device.execute(p.command)
            
class PresetEntry(models.Model):
    order = models.PositiveIntegerField(default = 1, help_text = "Commands are executed in order sorted by this field")
    device = models.ForeignKey(Device, help_text = "Device to command")
    command = models.ForeignKey(Command, default = 102, help_text = "Command to execute")
    presetDevice = models.ForeignKey(Preset, related_name = 'entries')

    def __unicode__(self):
        return self.device.__unicode__() + ' ' + self.command.__unicode__()


class SendSignalDevice(Device):
    signalOn = models.CharField(max_length=100, default = '', verbose_name = 'Signal to send at ON/Activate', help_text = "Write any string of characters, this will be sent to all signal handlers and if any is configured to listen it will react on it. If no listener is configured the signal will just be ignored after sending")
    signalOff = models.CharField(max_length=100, default = '', verbose_name = 'Signal to send at OFF', blank = True, help_text = "As above")

    def clean(self):
        if self.signalOff != '':
            self.activate = False
            self.onOff = True
        else:
            self.activate = True
            self.onOff = False

    def doExecute(self, cmd):
        if debug:
            print 'SendSignalDevice doExecute', self.name, cmd

        if cmd.cmd == 'ON' or cmd.cmd == 'ACTIVATE':
            postToQueue(self.signalOn, 'SendSignalDevice')
        elif cmd.cmd == 'OFF':
            if self.signalOff:
                postToQueue(self.signalOff, 'SendSignalDevice')

        return True

    def getConf(self):
        return ''

class WolDevice(Device):
    mac = models.CharField(max_length=17, default = '', verbose_name = 'MAC address to send to', help_text = "MAC address should be on the form: aa:bb:cc:00:11:22")
    ip = models.CharField(max_length=15, default = '', verbose_name = 'IP address to send to (blank for broadcast)', blank = True, help_text = "Generally leave this blank otherwise you know what to enter. Needs to be an IP address no domain name.")

    def clean(self):
        self.activate = True
        self.onOff = False
        if len(self.mac) != 17:
            raise ValidationError('MAC address needs to be 6 pair of digits hexadecimal (0-F) sepparated by : or -')

        self.mac = self.mac.replace('-', ':')
        try:
            for m in self.mac.split(':'):
                x = int(m, 16)
        except:
            raise ValidationError('MAC address needs to be 6 pair of digits hexadecimal (0-F) sepparated by : or -')

        if self.ip != '':
            ipParts = self.ip.split('.')
            if len(ipParts) != 4:
                raise ValidationError('IP address needs to be blank (for broadcast) or a valid ip address like "192.168.1.20"')
            try:
                for p in ipParts:
                    x = int(p, 10)
            except:
                raise ValidationError('IP address needs to be blank (for broadcast) or a valid ip address like "192.168.1.20"')
            

    def doExecute(self, cmd):
        if debug:
            print 'Wol Device doExecute', self.name, cmd
            print 'mac:', self.mac

            
        mac_raw = ''
        for m in self.mac.split(':'):
            x = int(m, 16)
            mac_raw += chr(x)


        wolpacket = '\xFF\xFF\xFF\xFF\xFF\xFF' + mac_raw * 16
        if debug:
            print 'MAC raw', repr(mac_raw)
            print 'WOL packet', repr(wolpacket)

        if self.ip == '':
            dest = '<broadcast>'
        else:
            dest = self.ip

        try:
            sock = socket(AF_INET, SOCK_DGRAM)
            sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
            sock.sendto(wolpacket, (dest, 9))
            sock.close()
        except:
            print 'Error sending WOL packet'
            if debug:
                raise

        return True

    def getConf(self):
        return ''

class VariableDelayedCommand(Device):
    device = models.ForeignKey(Device, related_name = 'dev', help_text = "Device to command")
    command = models.ForeignKey(Command, default = 102, help_text = "Command to do after the delay.")
    variable = models.ForeignKey('GlobalVariable', help_text = "The variable to evalute against. Need to be of type integer or float. Please note that the variable value is only evaluated once when this device is actually activated. Changes to the variable after that will not have any effect.")

    maxdelay = models.PositiveIntegerField(default = 60, help_text = "Maximum time in minutes that it may delay the command.")

    value1 = models.FloatField(help_text = "The first value to compare against. Togheter with value2 this will be a span. If Value1 < Value2 the delay will be larger for higher variable values. No delay for variable values < Value1 and maxdelay for variable values > Value2. Everything in between will be calculated linarly between Value1 and Value2.")
    value2 = models.FloatField(help_text = "The second value to compare against. Not allowed to be exactly the same as Value1.")
    
    def clean(self):
        self.activate = True
        self.onOff = False

        if float(self.value1) == float(self.value2):
            raise ValidationError('Value1 and Value2 cant be exactly the same value.')

        if self.variable.dataType not in [1, 2]:
            raise ValidationError('Variable needs to be of datatype Integer or Float')

    def doExecute(self, cmd):
        actualDelay = 0
        variableValue = float(self.variable.getValue())

        if self.value1 < self.value2: #Value is increasing during the delay
            if variableValue < self.value1:
                actualDelay = 0
            elif variableValue > self.value2:
                actualDelay = self.maxdelay
            else:
                actualDelay = int((variableValue-self.value1)*self.maxdelay/(self.value2-self.value1))

        else: #Value is decreasing during the delay
            if variableValue > self.value1:
                actualDelay = 0
                    
            elif variableValue < self.value2:
                actualDelay = self.maxdelay
            
            else:
                actualDelay = int((variableValue-self.value2)*self.maxdelay/(self.value1-self.value2))
        
        if actualDelay == 0:
            self.device.execute(self.command)
        elif actualDelay < 0:
            print 'Error actualDelay < 0', actualDelay
            print self.value1, self.value2, variableValue, self.maxdelay
        else:
            local = timezone(settings.TIME_ZONE)
            doAt = local.localize(datetime.datetime.now()) + datetime.timedelta(minutes = actualDelay)
            e = Event(device = self.device,
                      command = self.command,
                      doAt = doAt,
                      condition = None)
            e.save()
    
class ScheduledEvent(models.Model):
    RELATIVE_CHOICES = ((0, 'Absolute time'),
                        (1, 'Relative sunrise'),
                        (2, 'Relative sunset'))

    device = models.ForeignKey(Device, help_text = "Device to command when scheduled event happens.")
    command = models.ForeignKey(Command, help_text = "Command to execute on device when scheduled event happens.")


    relativeTo = models.PositiveIntegerField(default = 0, choices = RELATIVE_CHOICES)
    doAt = models.TimeField(verbose_name = 'Execution time', default = datetime.time(hour = 6, minute = 0), help_text = "Time of when the event should happen, not appliable if relative to sunrise/sunset.")

    relativeTime = models.IntegerField(default = 0, verbose_name = 'Minutes relative to sunset/sunrise', help_text = "Enter number of minutes in positivie or negative values.")

    doMonday = models.BooleanField(verbose_name = 'Monday')
    doTuesday = models.BooleanField(verbose_name = 'Tuesday')
    doWednesday = models.BooleanField(verbose_name = 'WednesDay')
    doThursday = models.BooleanField(verbose_name = 'Thursday')
    doFriday = models.BooleanField(verbose_name = 'Friday')
    doSaturday = models.BooleanField(verbose_name = 'Saturday')
    doSunday = models.BooleanField(verbose_name = 'Sunday')    

    condition = models.ForeignKey('GlobalVariable', blank = True, null = True, help_text = "When event has occured check this variable and only execute if it is true. True is any number not 0/0.0 or True.")

    def clean(self):
        if self.relativeTo > 0 and not EPHEM_AVAILABLE:
            raise ValidationError('pyephem not installed (correctly), that is required for relative to sunrise/sunset')
        if abs(self.relativeTime) > (8*60):
            raise ValidationError('Relative Time cant be greater than 8*60=480 minutes')
        if self.relativeTo == 0:
            self.realtiveTime = 0
        else:
            self.doAt = datetime.time(hour = 6, minute = 0)

    def __unicode__(self):
        weekdays = ''
        if self.doMonday:
            weekdays += 'M'
        else:
            weekdays += '-'

        if self.doTuesday:
            weekdays += 'T'
        else:
            weekdays += '-'
        if self.doWednesday:
            weekdays += 'W'
        else:
            weekdays += '-'
        if self.doThursday:
            weekdays += 'T'
        else:
            weekdays += '-'
        if self.doFriday:
            weekdays += 'F'
        else:
            weekdays += '-'
        if self.doSaturday:
            weekdays += 'S'
        else:
            weekdays += '-'
        if self.doSunday:
            weekdays += 'S'
        else:
            weekdays += '-'

        if self.relativeTime == 0:
            rt = ''
        elif self.relativeTime > 0:
            rt = '+ ' + str(self.relativeTime)
        else:
            rt = '- ' + str(abs(self.relativeTime))

        if self.relativeTo == 0:
            timeStr = self.doAt.isoformat()
        elif self.relativeTo == 1:
            timeStr = 'sunrise ' + rt
        else:
            timeStr = 'sunset ' + rt

        return self.device.__unicode__() + ' ' + self.command.__unicode__() + ' ' + weekdays + ' @ ' + timeStr

    def scheduleNext(self, earliest = None):
        local = timezone(settings.TIME_ZONE)
        now = local.localize(datetime.datetime.now())

        for e in Event.objects.filter(schedule = self):
            if debug:
                print 'Deleteing', e
            e.delete()

        if not earliest:
            earliest = now

        elif earliest < now:
            if debug:
                print 'Earliest in the past, that is not allowed'
            earliest = now

        weekdayNow = earliest.weekday()
        if debug:
            print 'Earliest:', earliest
            print 'Earliest day:', weekdayNow
            print 'Relative to:', self.relativeTo

        every = [self.doMonday,
                 self.doTuesday,
                 self.doWednesday,
                 self.doThursday,
                 self.doFriday,
                 self.doSaturday,
                 self.doSunday]

        if self.relativeTo == 0:
            if earliest.time() < self.doAt:
                timeDoAt = datetime.datetime.combine(earliest.date(), self.doAt)
            else:
                timeDoAt = datetime.datetime.combine(earliest.date(), self.doAt) + datetime.timedelta(days = 1)
        else:
            try:
                o=ephem.Observer()
                o.lat=str(getSetting('Location Latitude'))
                o.long=str(getSetting('Location Longitude'))
                o.date = ephem.Date(earliest - datetime.timedelta(minutes = self.relativeTime))
                s=ephem.Sun()
                s.compute()
                if self.relativeTo == 1:
                    timeDoAt = ephem.localtime(o.next_rising(s))
                elif self.relativeTo == 2:
                    timeDoAt = ephem.localtime(o.next_setting(s))
            except:
                print 'Error calculating sunset/sunrise based on location'
                raise


        daysToNext = (timeDoAt.date() - earliest.date()).days
        weekdayNow = timeDoAt.weekday()

        if debug:
            print 'timeDoAt', timeDoAt
            print 'Every:', every
            print 'WeekdayNow', weekdayNow
            print 'DaysToNext', daysToNext

        for i in range(weekdayNow, 12):
            if every[i%7]:
                break
            daysToNext += 1
        else:
            print 'Shouldnt be done any time if there is not scheduled events that shouldnt be done any day of the week - aborting scheduleNext()'
            return

        if debug:
            print 'Days to next', daysToNext

        if self.relativeTo == 0:
            newDateTime = local.localize(datetime.datetime.combine(earliest.date() + datetime.timedelta(days = daysToNext), self.doAt))

        elif self.relativeTo == 1:
            o.date = datetime.datetime.combine(earliest.date() + datetime.timedelta(days = (daysToNext - 1)), datetime.time(hour = 20, minute = 0))
            newDateTime = local.localize(ephem.localtime(o.next_rising(s))) + datetime.timedelta(minutes = self.relativeTime)
            if debug:
                print o.date
                                               

        elif self.relativeTo == 2:
            o.date = datetime.datetime.combine(earliest.date() + datetime.timedelta(days = daysToNext), datetime.time(hour = 8, minute = 0))
            newDateTime = local.localize(ephem.localtime(o.next_setting(s))) + datetime.timedelta(minutes = self.relativeTime)
            if debug:
                print o.date
            
        if debug:
            print 'Next event', newDateTime

        if newDateTime < now:
            print '\n\nBAD DATE, already passed\n'
            if debug:
                raise ValueError('BAD DATE, already passed')

        e = Event(device = self.device, command = self.command, doAt = newDateTime, schedule = self, condition = self.condition)
        e.save()




class Event(models.Model):
    device = models.ForeignKey(Device)
    command = models.ForeignKey(Command)
    doAt = models.DateTimeField()
    condition = models.ForeignKey('GlobalVariable', blank = True, null = True)

    schedule = models.ForeignKey(ScheduledEvent, null = True, blank = True, default = None)

    def __unicode__(self):
        fmt = '%Y-%m-%d %H:%M:%S'
        local = timezone(settings.TIME_ZONE)
        return self.device.name + ' ' + self.command.__unicode__() + ' ' + self.doAt.astimezone(local).strftime(fmt)

    def delay(self, minutes):
        self.doAt = self.doAt + datetime.timedelta(minutes = minutes)
        self.save()

    def save(self):
        super(Event, self).save()        
        postToQueue('timedevent,changed', 'system')

    def delete(self):
        super(Event, self).delete()        
        postToQueue('timedevent,changed', 'system')
        

class Threshold(models.Model):
    THRESHOLD_TYPE = ((0, 'Below threshold'),
                      (1, 'Above threshold'),
                      (2, 'Passing threshold'),
                      (3, 'Any changes'))

    globalVariable = models.ForeignKey('GlobalVariable', help_text = "Variable to evaluate")
    threshold = models.CharField(max_length=30, help_text = "Threshold value, should be of same datatype as the variable above. I.e use -1.0 for float and -1 for integer.")
    type = models.IntegerField(default = 0, choices = THRESHOLD_TYPE, help_text = "Select type of threshold.")
    
    device = models.ForeignKey(Device, help_text = "Device to command when threashold is triggered.")
    command = models.ForeignKey(Command, help_text = "Command to execute when threashold is triggered.")

    def __unicode__(self):
        s = self.globalVariable.name + ' '
        if self.type == 0:
            s += ' going below ' + self.threshold
        elif self.type == 1:
            s += ' going above ' + self.threshold
        elif self.type == 2:
            s += ' passing ' + self.threshold
        elif self.type == 3:
            s += ' changes'

        s += ' do ' + self.command.cmd + ' on ' + self.device.name
        return s

    def clean(self):
        if self.globalVariable.dataType in [0, 3]: #String or boolean
            if self.type != 3:
                raise ValidationError('For GlobalVariables of datatype String/Boolean only type "Any changes" is allowed.')
        elif self.globalVariable.dataType == 1: #Integer
            try:
                x = int(self.threshold)
                self.threshold = str(x)
            except:
                raise ValidationError('Cant parse threshold value as an integer')
        elif self.globalVariable.dataType == 2: #Float
            try:
                x = float(self.threshold)
                self.threshold = str(x)
            except:
                raise ValidationError('Cant parse threshold value as a float')


        if self.command.cmd == 'DIM':
            if not self.device.dim:
                raise ValidationError('Dimming not supported by selected device')

        elif self.command.cmd == 'ACTIVATE':
            if not self.device.activate:
                raise ValidationError('Activate not supported by selected device')

        elif self.command.cmd in ['ON', 'OFF']:
            if not self.device.onOff:
                raise ValidationError('On/Off not supported by selected device')

        if self.command.cmd not in ['ON', 'OFF', 'ACTIVATE', 'DIM']:
            raise ValidationError('Selected command is not supported by selected device')


    def save(self):
        if self.globalVariable.dataType in [0, 3]: #String or boolean
            self.threshold = ''

        super(Threshold, self).save()


    def check(self, value, previousValue):
        #print 'Check called,', value, previousValue, self.type, self.globalVariable.dataType, self.threshold
        if self.type == 0: #Below
            #print 'Below'
            if self.globalVariable.dataType == 1: #Integer
                #print 'Integer'
                if previousValue == None or (int(self.threshold) > int(value) and int(self.threshold) < int(previousValue)):
                    pass
                    #print 'Threshold triggered'
                else:
                    return
            elif self.globalVariable.dataType == 2: #Float
                #print 'Float'
                if previousValue == None or (float(self.threshold) > float(value) and float(self.threshold) < float(previousValue)):
                    pass
                    #print 'Threshold triggerd'
                else:
                    return
        elif self.type == 1: #Above
            #print 'Above'
            if self.globalVariable.dataType == 1: #Integer
                #print 'Integer'
                if previousValue == None or (int(self.threshold) < int(value) and int(self.threshold) > int(previousValue)):
                    pass
                    #print 'Threshold triggered'
                else:
                    return
            elif self.globalVariable.dataType == 2: #Float
                #print 'Float'
                if previousValue == None or (float(self.threshold) < float(value) and float(self.threshold) > float(previousValue)):
                    pass
                    #print 'Threshold triggerd'
                else:
                    return

        elif self.type == 2: #Passing
            #print 'Passing'
            if self.globalVariable.dataType == 1: #Integer
                #print 'Integer'
                if (previousValue == None) or (int(self.threshold) < int(value) and int(self.threshold) > int(previousValue)) or (int(self.threshold) > int(value) and int(self.threshold) < int(previousValue)):
                    pass
                    #print 'Threshold triggered'
                else:
                    return
            elif self.globalVariable.dataType == 2: #Float
                #print 'Float'
                if previousValue == None or (float(self.threshold) < float(value) and float(self.threshold) > float(previousValue)) or (float(self.threshold) > float(value) and float(self.threshold) < float(previousValue)):
                    pass
                    #print 'Threshold triggerd'
                else:
                    return

        elif self.type == 3: #Changing
            #print 'Changing'
            if previousValue == None or value != previousValue:
                pass
                #print 'Threshold triggerd'
            else:
                return

        #If ending up here threshold is triggered        
        try:
            postToQueue('threshold_triggerd,' + str(self.id) + ':' + str(value) + ',' + str(previousValue), 'system')
        except:
            print 'Error posting signal of Threshold triggered'
        
        self.device.execute(self.command)

FETCH_FILE = 0
FETCH_URL = 1
FETCH_PROGRAM = 2


FETCHTYPES = ((FETCH_FILE, 'File'),
              (FETCH_URL, 'URL'),
#              (FETCH_PROGRAM, 'Execute')
              )

PARSE_LINE_CHAR_SIZE = 0
PARSE_LINE_SCANF = 1
PARSE_LINE_REGEXP = 2
PARSE_REGEXP = 3
PARSE_XML = 4
PARSE_JSON = 5


PARSERTYPES = ((PARSE_LINE_CHAR_SIZE, 'Line,Character position,Length'),
               (PARSE_LINE_SCANF, 'Line,Scanf'),
               (PARSE_LINE_REGEXP, 'Line,Regexp'),
               (PARSE_REGEXP, 'Regexp'),
               (PARSE_XML, 'XML (Xpath)'),
#               (PARSE_JSON, 'JSON')
               )
               
re_cache = {}
re_cache_size = 500

class DataFetcher(models.Model):
    name = models.CharField(max_length=30)

    enable = models.BooleanField(default = True, blank = True, help_text = "Unset if you want to temporarly disable this fetcher.")

    fetchType = models.IntegerField(default = FETCH_FILE, choices = FETCHTYPES, help_text = "Select type of fetch, please note that each type has its own set of options below.")
    fetchInterval = models.PositiveIntegerField(default = 0, help_text = "Number of minutes between each fetch. Minimum 1. This is not exact, it will vary somewhat depending on load and total number of fetch jobs.")

    parserType = models.IntegerField(default = PARSE_LINE_CHAR_SIZE, choices = PARSERTYPES, help_text = "Select type of parser, please note that each type has its own set of required options below.")

    signalPrefix = models.CharField(max_length=30, default = '', blank = True, help_text = "Generated signal will allways start with this string.")    
    
    fileName = models.CharField(max_length=255, default = '', blank = True, help_text = "Absolute path to the file to read")

    url = models.CharField(max_length=255, default = '', blank = True, help_text = "HTTP URL to fetch.")
    headers = models.CharField(max_length=255, default = '', blank = True, help_text = "Special headers in the HTTP request. Format key=value sepparated by ;")

    executeable = models.CharField(max_length=255, default = '', blank = True, help_text = "Absolute path to the file to execute.")

    line = models.PositiveIntegerField(default = 0, help_text = "Line number in the fetch data to use.")
    char = models.PositiveIntegerField(default = 0, help_text = "Character position in the line to use.")
    size = models.PositiveIntegerField(default = 1, help_text = "Number of Characters to read starting at char. 0 means the rest of the line.")

    scanf = models.CharField(max_length=255, default = '', blank = True, help_text = "Scanf string to use to fetch the information.")
    regexp = models.CharField(max_length=255, default = '', blank = True, help_text = "Regexp string to use to fetch the information. This does not have to match beginning of data, just somewhere in the data.")

    regexpcompiled = models.CharField(max_length=255, default = '', blank = True)

    parsedVariable = models.PositiveIntegerField(default = 1, help_text = "Variable to use if more than one is parsed with scanf/regexp.")

    path = models.CharField(max_length=255, default = '', blank = True, help_text = "Xpath to use. For xPath instructions see <a href=\"http://docs.python.org/2/library/xml.etree.elementtree.html#xpath-support\">python docs on the subject</a>. Please note that this only suport a subset of xPath standard, this is described in the linked document.")

    def __unicode__(self):
        if self.fetchType == FETCH_FILE:
            s = self.fileName
        elif self.fetchType == FETCH_URL:
            s = self.url
        elif self.fetchType == FETCH_EXECUTE:
            s = self.executable
        else:
            s = '--unknown--'
            
        return self.name + ' fetch every ' + str(self.fetchInterval) + ' min from ' + s

    def clean(self):
        if self.fetchType == FETCH_FILE:
            if self.fileName == '':
                raise ValidationError('FileName not allowed to be empty')
            elif not os.path.isfile(self.fileName):
                raise ValidationError('File doesnt exist')
            
        elif self.fetchType == FETCH_URL:
            if self.url == '':
                raise ValidationError('URL not allowed to be empty')
            if not self.url.startswith('http://'):
                self.url = 'http://' + self.url
        elif self.fetchType == FETCH_EXECUTE:
            if self.executable == '':
                raise ValidationError('Executable not allowed to be empty')


        if self.parserType == PARSE_LINE_CHAR_SIZE:
            if self.size < 0 or self.size > 100:
                raise ValidationError('Size needs to be between 0 and 100, 0 means to end of line. Whitespaces in the beginning and/or end will be removed.')

        elif self.parserType == PARSE_LINE_SCANF:
            if self.scanf == '':
                raise ValidationError('Scanf not allowed to be empty')

            try:
                self.regexpcompiled = scanf_compile(self.scanf)[0]
            except:
                raise ValidationError('Bad format in scanf-string')

        elif self.parserType == PARSE_LINE_REGEXP:
            if self.regexp == '':
                raise ValidationError('Regexp not allowed to be empty')

            try:
                _t = re.compile(self.regexp)
                self.regexpcompiled = self.regexp
            except:
                raise ValiedtionError('Bad format in regexp')

        elif self.parserType == PARSE_REGEXP:
            if self.regexp == '':
                raise ValidationError('Regexp not allowed to be empty')

            try:
                _t = re.compile(self.regexp)
                self.regexpcompiled = self.regexp
            except:
                raise ValidationError('Bad format in regexp')


        elif self.parserType == PARSE_XML:
            if self.path == '':
                raise ValidationError('Path is not allowed to be empty')
            pass
        elif self.parserType == PARSE_JSON:
            raise ValidationError('Parse JSON not implemented yet')


        try:
            fetchData = self.fetchData(verify = True)
        except:
            strings = traceback.format_exception_only(sys.exc_type, sys.exc_value)
            if debug:
                print strings
            raise ValidationError('Unable to fetch. Error given:' + strings[0].strip())

        try:
            fetchResult = self.parse(fetchData, verify = True)
            print 'Result of fetch and parse is:', fetchResult
        except:
            strings = traceback.format_exception_only(sys.exc_type, sys.exc_value)
            if debug:
                print strings
            raise ValidationError('Unable to parse. Error given:' + strings[0].strip())

    def save(self):
        super(DataFetcher, self).save()
        postToQueue('datafetcher,configuration,changed', 'system')

    def fetch(self):
        success = True
        try:
            fetchResult = self.parse(self.fetchData(verify = False), verify = False)
            
            if debug:
                print 'Fetch result', fetchResult
        except:
            fetchResult = traceback.format_exception_only(sys.exc_type, sys.exc_value)[0]
            success = False

        sig = self.compileSignal(fetchResult, success)

        postToQueue(sig, 'datafetcher')

    def compileSignal(self, fetchResult, success):
        if success:           
            if self.signalPrefix == '':
                sig = 'datafetcher,' + str(self.id) + ',' + str(self.name) + ',fetched:' + fetchResult
            else:
                sig = self.signalPrefix + fetchResult

        else:
            sig = 'datafetcher,' + str(self.id) + ',' + str(self.name) + ',failed:' + fetchResult

        return sig

    def fetchData(self, verify = False):
        if self.fetchType == FETCH_FILE:
            if debug:
                print 'FETCH_FILE', self.fileName

            try:
                return open(self.fileName, 'r').read()
            except IOError, e:
                if verify:
                    raise
                print 'Failed reading from file:', self.fileName
                print e.errno, e

            except:
                if verify:
                    raise
                print 'Other error reading from file:', self.fileName
                if debug:
                    raise
            
            return ''

        elif self.fetchType == FETCH_URL:
            headersDict = {}
            if self.headers != '':
                try:
                    for h in self.headers.split(';'):
                        _t = h.split('=')
                        headersDict[_t[0]] = _t[1]
                except:
                    if debug:
                        print 'Error in fetch url, header creating'
                    if verify:
                        raise

            if debug:
                print 'Headers:'
                for k in headersDict:
                    print '  ', k, ':', headersDict[k]

            try:
                if headersDict:
                    req = urllib2.Request(self.url, headers = headersDict)
                else:
                    req = urllib2.Request(self.url)

                d = urllib2.urlopen(req).read()
            except:
                if verify:
                    raise

            return d
            
        elif self.fetchType == FETCH_EXECUTE:
            print 'Not implemented yet'
            return ''

        print 'Unknown fetchtype', self.fetchType
        return ''


    def parse(self, data, verify = False):
        if debug:
            print 'Parse ******************\n', data

        if data == '':
            return None

        if self.parserType == PARSE_LINE_CHAR_SIZE:
            lines = data.split('\n')
            try:
                if self.size == 0:
                    return lines[self.line][self.char:].strip()
                return lines[self.line][self.char:self.char+self.size].strip()
            except:
                if debug:
                    print 'Failed parsing', self.line, self.char, self.size
                    print lines
                if verify or debug:
                    raise
                return None

        elif self.parserType == PARSE_LINE_SCANF or self.parserType == PARSE_LINE_REGEXP or self.parserType == PARSE_REGEXP:

            if self.parserType == PARSE_REGEXP:
                d = data
            else:
                d = data.split('\n')[self.line]

            if self.regexpcompiled not in re_cache:
                format_re = re.compile(self.regexpcompiled)

                if len(re_cache) > re_cache_size:
                    if debug:
                        print 'RE cache full, clearing it'
                    re_cache.clear()
                re_cache[self.regexpcompiled] = format_re
                        
            try:
                found = re_cache[self.regexpcompiled].search(d)
                if found == None:
                    raise ValueError('Regular expression didnt match anything')
                return found.groups()[self.parsedVariable - 1]                
            except:
                if debug:
                    print 'Failed parsing', self.regexpcompiled, self.scanf, self.parsedVariable
                    print d
                    print '*********************'
                    print data
                    print '*********************'
                if verify or debug:
                    raise

                return None

        elif self.parserType == PARSE_XML:
            try:
                root = ET.fromstring(data)
                res = root.findall(self.path)
                if debug:
                    print 'XML fetch', res[0].text
            
                return res[0].text
            except:
                if debug:
                    print 'Failed parsing XML', self.path
                    print data
                if verify or debug:
                    raise

                return None

        elif self.parserType == PARSE_JSON:
            pass
        else:
            print 'Unknown parsetype', self.parserType

        return None


DATATYPES = ((0, 'String/Not specified'),
             (1, 'Integer'),
             (2, 'Float'),
             (3, 'Boolean'))


class GlobalVariable(models.Model):

    name = models.CharField(max_length=30)
    unit = models.CharField(max_length=10, default = '', blank = True, help_text = "Text to display after the variable value for example C or F")
    hidden = models.BooleanField(default = True, verbose_name = 'Hide from display')
    dataType = models.IntegerField(default = 0, choices = DATATYPES, help_text = "Select datatype, please note that variables that is if type String is not possible to keep history on or plot in any graph.")
    maxage = models.PositiveIntegerField(default = 0, help_text = "Number of minutes that maximum should go since last update before the value is flaged as old. This will be marked by greyed out values in the jquery remote theme. Set this to 0 (default) to disable flaging as old.")
    source = models.CharField(max_length=255, default = '', blank = True)

    def __unicode__(self):
        fmt = '%H:%M:%S'
        local = timezone(settings.TIME_ZONE)
        
        return self.name + u':' + self.getValue() + ' (' + self.getUpdated().astimezone(local).strftime(fmt) + ')'

    def delete(self):
        key = 'GV' + str(self.id)
        super(GlobalVariable, self).delete()

        print 'Delete all stored data for this Global variable'
        ValueHistoryMem.objects.filter(key = key).delete()
        ValueHistory.objects.filter(key = key).delete()
        CurrentValue.objects.filter(key = key).delete()
        print 'Delete done'
        


    def getValue(self):        
        c = CurrentValue.objects.filter(key = 'GV'+str(self.id))
        if len(c) == 0:
            if self.dataType == 1:
                return '0'
            elif self.dataType == 2:
                return '0.0'
            elif self.dataType == 3:
                return 'False'
            else:
                return ''
        else:
            return c[0].value

    def getUpdated(self):
        c = CurrentValue.objects.filter(key = 'GV'+str(self.id))
        if len(c) == 0:
            local = timezone(settings.TIME_ZONE)
            return local.localize(datetime.datetime(year=1980,month=8,day=3,hour=0,minute=0))
        else:
            return c[0].lastUpdated

    def getOld(self):
        local = timezone(settings.TIME_ZONE)
        now = local.localize(datetime.datetime.now())
        updated = self.getUpdated()
        age = now - updated
        if self.maxage > 0 and self.maxage < ((age.days * 24 * 60) + (age.seconds/60)):
            return True, updated
        return False, updated

    def updateValue(self, value, allowHistory = True):
        if self.dataType == 1:
            try:
                x = int(value)
                value = str(x)
            except:
                value = '-9999'

        elif self.dataType == 2:
            try:
                x = float(value)
                value = str(x)
            except:
                value = '-9999.99'

        elif self.dataType == 3:
            try:
                if value.lower() in ['true', 't', '1', 'on', 'yes', 'y']:
                    value = 'True'
                else:
                    value = 'False'
            except:
                value = 'False'

        previousValue = None
        c = CurrentValue.objects.filter(key = 'GV'+str(self.id))
        if len(c) == 0:
            c = CurrentValue(key = 'GV'+str(self.id), value = value, dataType = self.dataType)
            c.save(allowHistory)
        else:
            previousValue = c[0].value
            c[0].value = value
            c[0].dataType = self.dataType
            c[0].save(allowHistory)

        try:
            if allowHistory:
                postToQueue('variable_changed,' + self.name + ':' + str(value), 'system')
            else:
                postToQueue('variable_changed,' + self.name + ':--not allowed--', 'system')
        except:
            print 'Error posting signal of Global Variable value changed'

        for th in Threshold.objects.filter(globalVariable = self):
            th.check(value, previousValue)



class CurrentValue(models.Model):
    key = models.CharField(max_length=10, blank = True)
    value = models.CharField(max_length=30)
    dataType = models.IntegerField(default = 0, choices = DATATYPES)
    lastUpdated = models.DateTimeField(auto_now = True)        

    def __unicode__(self):
        return self.key + ':' + self.value + ' @ ' + self.lastUpdated.isoformat(' ')

    def save(self, allowHistory = True):
        super(CurrentValue, self).save()

        try:
            if allowHistory:
                postToQueue('currentvalue_changed,' + self.key + ':' + str(self.value), 'system')
        except:
            print 'Error posting signal of Global Variable value changed'


        if not allowHistory:
            return

        if self.dataType == 0:
            if debug:
                print 'Dont do anything datatype = 0'
            #Dont do any history for String datatype
            return

        if self.dataType == 3: #Boolean datatype, change datatype to Integer for plotting possibility
            if self.value == 'True':
                self.value = 1
            else:
                self.value = 0

            self.dataType = 1

        v = ValueHistoryMem(key = self.key,
                            value = self.value,
                            dataType = self.dataType,
                            timestamp = self.lastUpdated)
        v.save()

        currentHour = self.lastUpdated.replace(minute = 0, second = 0, microsecond = 0)
        previousHour = currentHour - datetime.timedelta(hours = 1)

        previous24h = currentHour - datetime.timedelta(hours = 24)
        
        if debug:
            print 'Current', currentHour
            print 'Prev', previousHour
            print 'prev24h', previous24h


        #Erase all entries that is older than previous hour
        ValueHistoryMem.objects.filter(key = self.key).filter(timestamp__lt = previous24h).delete()

        if self.key[:2] != 'GV':
            if debug:
                print 'Dont store old history in database for this key type', self.key
            return

        if self.dataType == 0:
            if debug:
                print 'Dont store old history in database for string datatype'
            return

        try:
            ValueHistory.objects.get(key = self.key, timestamp = previousHour)
            if debug:
                print 'Dont need to do anything, average has already been calculated'
            return
        except:
            pass

        vs = ValueHistoryMem.objects.filter(key = self.key).filter(timestamp__lt = currentHour).filter(timestamp__gte = previousHour)

        count = 0
        sum = 0.0
        for v in vs:
            if debug:
                print 'Avg of', v.timestamp, v.value

            count += 1
            if v.dataType == 1:
                sum += int(v.value)
            elif v.dataType == 2:
                sum += float(v.value)
            elif v.dataType == 3:
                if v.value == 'True':
                    sum += 1
            
        if debug:
            print 'count', count, 'sum', sum
        if count > 0:
            avg = sum/count
            h = ValueHistory(key = self.key, value = str(avg), dataType = 2, timestamp = previousHour)
            h.save()
            if debug:
                print 'New average value saved', self.key, avg
            
        else:
            if debug:
                print 'No entries to calcualte average on'
        

class ValueHistoryMem(models.Model):
    key = models.CharField(max_length=10, blank = True)
    value = models.CharField(max_length=30)
    dataType = models.IntegerField(default = 0, choices = DATATYPES)
    timestamp = models.DateTimeField()

    def __unicode__(self):
        return self.key + ':' + self.value + ' @ ' + self.timestamp.isoformat(' ')


class ValueHistory(models.Model):
    key = models.CharField(max_length=10, blank = True)
    value = models.CharField(max_length=30)
    dataType = models.IntegerField(default = 0, choices = DATATYPES)
    timestamp = models.DateTimeField()

    def __unicode__(self):
        return self.key + ':' + self.value + ' @ ' + self.timestamp.isoformat(' ')

from signals.models import postToQueue


@receiver(post_save, dispatch_uid=__name__, sender=ScheduledEvent)
def scheduledEventChanged(sender, **kwargs):
    kwargs['instance'].scheduleNext()


