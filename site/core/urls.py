from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('core.views',
    url(r'^$', 'index', kwargs = {'admin': False}),
    url(r'admin/^$', 'index', kwargs = {'admin': True}),

    url(r'^(?P<device_id>\d+)/$', 'detail'),

    url(r'^(?P<device_id>\d+)/(?P<cmd_id>\d+)/$', 'do', kwargs = {'learn_admin': False, 'rest': False}),
    url(r'^learn/(?P<device_id>\d+)/(?P<cmd_id>\d+)/$', 'do', kwargs = {'learn_admin': True, 'rest': False}),
    url(r'^rest/(?P<device_id>\d+)/(?P<cmd_id>\d+)/$', 'do', kwargs = {'learn_admin': False, 'rest': True}),

    url(r'^rest/do/$', 'restdo'),

    url(r'^rest/devices/$', 'rest_devices'),

    url(r'^rest/gv/all/$', 'globalvariable'),
    url(r'^rest/gv/(?P<gv_id>\d+)/$', 'globalvariable'),

    url(r'^rest/dev/all/$', 'devicestate'),
    url(r'^rest/dev/(?P<dev_id>\d+)/$', 'devicestate'),
    url(r'^rest/events/$', 'events_rest'),

    url(r'^rest/$', 'restdoc'),

    url(r'^stats/24h/(?P<key>\S+).png$', 'plot'),

    url(r'^about/$', 'about'),
    url(r'^revision/$', 'revision'),

    url(r'^events/$', 'event_index', kwargs = {'admin': False}),
    url(r'^events/admin/$', 'event_index', kwargs = {'admin': True}),

    url(r'^events/(?P<event_id>\d+)/$', 'event', kwargs = {'admin': False}),
    url(r'^events/admin/(?P<event_id>\d+)/$', 'event', kwargs = {'admin': True}),

    url(r'^events/(?P<event_id>\d+)/delay/(?P<minutes>\d+)/$', 'delay_event', kwargs = {'admin': False}),
    url(r'^events/admin/(?P<event_id>\d+)/delay/(?P<minutes>\d+)/$', 'delay_event', kwargs = {'admin': True}),

    url(r'^events/(?P<event_id>\d+)/cancel$', 'cancel_event', kwargs = {'admin': False}),
    url(r'^events/admin/(?P<event_id>\d+)/cancel$', 'cancel_event', kwargs = {'admin': True}),

    url(r'^learn/$', 'learn', kwargs = {'admin': False}),
    url(r'^learn/admin/$', 'learn', kwargs = {'admin': True}),

    url(r'^backup/$', 'backup'),
    url(r'^backup/do/$', 'doBackup'),
    url(r'^backup/done/$', 'backupDone'),
    url(r'^backup/dorestore/(?P<backup>\S+)/$', 'restoreBackup'),
    url(r'^backup/(?P<backup>\S+)/$', 'showBackup'),

    url(r'^update/do/$', 'doUpdate'),
    url(r'^update/done/$', 'updateDone'),

    url(r'^viewlogs/$', 'viewLogs'),

    url(r'^read_config/$', 'read_config'),
)
