# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'GlobalVariable.maxage'
        db.add_column('core_globalvariable', 'maxage',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting field 'GlobalVariable.maxage'
        db.delete_column('core_globalvariable', 'maxage')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.currentvalue': {
            'Meta': {'object_name': 'CurrentValue'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'lastUpdated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'core.event': {
            'Meta': {'object_name': 'Event'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'condition': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']", 'null': 'True', 'blank': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'doAt': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['core.ScheduledEvent']", 'null': 'True', 'blank': 'True'})
        },
        'core.globalvariable': {
            'Meta': {'object_name': 'GlobalVariable'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maxage': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'source': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10', 'blank': 'True'})
        },
        'core.groupdevice': {
            'Meta': {'object_name': 'GroupDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevices': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subdevices'", 'symmetrical': 'False', 'to': "orm['core.Device']"})
        },
        'core.preset': {
            'Meta': {'object_name': 'Preset', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'})
        },
        'core.presetentry': {
            'Meta': {'object_name': 'PresetEntry'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'presetDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': "orm['core.Preset']"})
        },
        'core.rawtellstickdevice': {
            'Meta': {'object_name': 'RawTellstickDevice', '_ormbases': ['core.Device']},
            'code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'controllingSameDeviceAs': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'related_name': "'controllingSameDeviceAs_rel_+'", 'null': 'True', 'blank': 'True', 'to': "orm['core.RawTellstickDevice']"}),
            'deviceId': ('django.db.models.fields.PositiveIntegerField', [], {'default': '9999'}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'rawName': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'unit': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'})
        },
        'core.scheduledevent': {
            'Meta': {'object_name': 'ScheduledEvent'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'condition': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']", 'null': 'True', 'blank': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'doAt': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(6, 0)'}),
            'doFriday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doMonday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doSaturday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doSunday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doThursday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doTuesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doWednesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'relativeTime': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'relativeTo': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'core.sendsignaldevice': {
            'Meta': {'object_name': 'SendSignalDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'signalOff': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'signalOn': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        'core.threshold': {
            'Meta': {'object_name': 'Threshold'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'globalVariable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'threshold': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'core.timerdevice': {
            'Meta': {'object_name': 'TimerDevice', '_ormbases': ['core.Device']},
            'clearOtherEvents': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'condition': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']", 'null': 'True', 'blank': 'True'}),
            'delayMinutes': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subdevice'", 'to': "orm['core.Device']"})
        },
        'core.valuehistory': {
            'Meta': {'object_name': 'ValueHistory'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'core.valuehistorymem': {
            'Meta': {'object_name': 'ValueHistoryMem'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'core.woldevice': {
            'Meta': {'object_name': 'WolDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '15', 'blank': 'True'}),
            'mac': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '17'})
        }
    }

    complete_apps = ['core']