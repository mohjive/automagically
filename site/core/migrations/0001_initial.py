# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Event'
        db.create_table('core_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.IntegerField')()),
            ('command', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('core', ['Event'])

        # Adding model 'Command'
        db.create_table('core_command', (
            ('id', self.gf('django.db.models.fields.PositiveIntegerField')(primary_key=True)),
            ('cmd', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('argInt', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('core', ['Command'])

        # Adding model 'Device'
        db.create_table('core_device', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
            ('htmlText', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
            ('activate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('onOff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('dim', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['Device'])

        # Adding model 'RawDevice'
        db.create_table('core_rawdevice', (
            ('device_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['core.Device'], unique=True, primary_key=True)),
            ('deviceId', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('core', ['RawDevice'])

        # Adding model 'TimerDevice'
        db.create_table('core_timerdevice', (
            ('device_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['core.Device'], unique=True, primary_key=True)),
            ('subDevice', self.gf('django.db.models.fields.related.ForeignKey')(related_name='subdevice', to=orm['core.Device'])),
            ('delayMinutes', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('command', self.gf('django.db.models.fields.related.ForeignKey')(default=102, to=orm['core.Command'])),
        ))
        db.send_create_signal('core', ['TimerDevice'])

        # Adding model 'GroupDevice'
        db.create_table('core_groupdevice', (
            ('device_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['core.Device'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('core', ['GroupDevice'])

        # Adding M2M table for field subDevices on 'GroupDevice'
        db.create_table('core_groupdevice_subDevices', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('groupdevice', models.ForeignKey(orm['core.groupdevice'], null=False)),
            ('device', models.ForeignKey(orm['core.device'], null=False))
        ))
        db.create_unique('core_groupdevice_subDevices', ['groupdevice_id', 'device_id'])

    def backwards(self, orm):
        # Deleting model 'Event'
        db.delete_table('core_event')

        # Deleting model 'Command'
        db.delete_table('core_command')

        # Deleting model 'Device'
        db.delete_table('core_device')

        # Deleting model 'RawDevice'
        db.delete_table('core_rawdevice')

        # Deleting model 'TimerDevice'
        db.delete_table('core_timerdevice')

        # Deleting model 'GroupDevice'
        db.delete_table('core_groupdevice')

        # Removing M2M table for field subDevices on 'GroupDevice'
        db.delete_table('core_groupdevice_subDevices')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'htmlText': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'core.event': {
            'Meta': {'object_name': 'Event'},
            'command': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'device': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'core.groupdevice': {
            'Meta': {'object_name': 'GroupDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevices': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subdevices'", 'symmetrical': 'False', 'to': "orm['core.Device']"})
        },
        'core.rawdevice': {
            'Meta': {'object_name': 'RawDevice', '_ormbases': ['core.Device']},
            'deviceId': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'})
        },
        'core.timerdevice': {
            'Meta': {'object_name': 'TimerDevice', '_ormbases': ['core.Device']},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'delayMinutes': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subdevice'", 'to': "orm['core.Device']"})
        }
    }

    complete_apps = ['core']