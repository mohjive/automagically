# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ScheduledEvent'
        db.create_table('core_scheduledevent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Device'])),
            ('command', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Command'])),
            ('doAt', self.gf('django.db.models.fields.TimeField')()),
            ('nextEvent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Event'], null=True)),
            ('doMonday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('doTuesday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('doWednesday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('doThursday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('doFriday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('doSaturday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('doSunday', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['ScheduledEvent'])

    def backwards(self, orm):
        # Deleting model 'ScheduledEvent'
        db.delete_table('core_scheduledevent')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'htmlText': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'core.event': {
            'Meta': {'object_name': 'Event'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'doAt': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'core.groupdevice': {
            'Meta': {'object_name': 'GroupDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevices': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subdevices'", 'symmetrical': 'False', 'to': "orm['core.Device']"})
        },
        'core.preset': {
            'Meta': {'object_name': 'Preset', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'})
        },
        'core.presetentry': {
            'Meta': {'object_name': 'PresetEntry'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'presetDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': "orm['core.Preset']"})
        },
        'core.rawdevice': {
            'Meta': {'object_name': 'RawDevice', '_ormbases': ['core.Device']},
            'code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'deviceId': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'protocolModel': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'rawName': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'unit': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'})
        },
        'core.scheduledevent': {
            'Meta': {'object_name': 'ScheduledEvent'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'doAt': ('django.db.models.fields.TimeField', [], {}),
            'doFriday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doMonday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doSaturday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doSunday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doThursday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doTuesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'doWednesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nextEvent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Event']", 'null': 'True'})
        },
        'core.timerdevice': {
            'Meta': {'object_name': 'TimerDevice', '_ormbases': ['core.Device']},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'delayMinutes': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subdevice'", 'to': "orm['core.Device']"})
        }
    }

    complete_apps = ['core']