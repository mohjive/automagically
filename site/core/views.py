from django.http import HttpResponse, Http404, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from core.models import Device, Command, RawTellstickDevice, GroupDevice, TimerDevice, Preset, Event, GlobalVariable, CurrentValue, ValueHistoryMem, ValueHistory
from signals.models import postToQueue
from django.conf import settings
from django.core import serializers
from django.utils import simplejson
from operator import itemgetter, attrgetter
from django import forms
from django.contrib.admin import widgets
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt

import time
import datetime
import traceback
import sys
import os
import subprocess
from pytz import timezone

try:
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure
    from matplotlib.dates import DateFormatter
    from matplotlib.ticker import MaxNLocator
    MATPLOTLIB_AVAILABLE = True
except:
    print 'Failed importing matplotlib, make sure it is installed'
    MATPLOTLIB_AVAILABLE = False

debug = False

#This is hardcoded from Command model (for faster access as it never changes)
cmds = {'DIM': range(0, 101, 10), #End value is max+1 to make sure last value is included
        'ON': 101,
        'OFF': 102,
        'BELL': 103,
        'LEARN': 104,
        'ACTIVATE': 105}

iphoneDimMap = [(20, 47, 10),
                (48, 75, 20),
                (76, 103, 30),
                (104, 131, 40),
                (132, 159, 50),
                (160, 187, 60),
                (188, 215, 70),
                (216, 243, 80),
                (244, 271, 90),
                (272, 299, 100),
]
        
def getViewType(request):
    viewType = 'default'

    if 'iPhone' in request.META['HTTP_USER_AGENT']:
        viewType = 'iphone'

    viewType = 'core/' + viewType + '.html'
    return viewType

@cache_control(max_age=99999)
def index(request, admin):
    global cmds
    
    devList = Device.objects.filter(hidden = False).order_by('order')
    varList = GlobalVariable.objects.filter(hidden = False)

    if admin:
        template = 'admin/core_index.html'
    else:
        template = 'core/index.html'

    return render_to_response(template, {'viewType': getViewType(request), 'devList': devList, 'varList': varList, 'cmds': cmds, 'iphoneDimMap': iphoneDimMap})

class CreateEventForm(forms.ModelForm):
    class Meta:
        model = Event
        exclude = ('device', 'schedule')
    
def detail(request, device_id):
    dev = get_object_or_404(Device, pk=device_id)
    if request.method == 'POST':
        form = CreateEventForm(request.POST)

        if form.is_valid():
            e = form.save(commit = False)
            e.device = dev
            e.save()
            return redirect('/core/')

    else:
        form = CreateEventForm(initial={'device': dev.id, 'doAt': datetime.datetime.now()})
    
    return render(request, 'core/detail.html', {'viewType': getViewType(request), 'dev': dev, 'form': form})

def about(request):
    return render(request, 'admin/core_about.html')

def revision(request):
    try:
        p =subprocess.Popen('git rev-list --max-count=1 HEAD', stdout = subprocess.PIPE, cwd = '/home/pi/source/automagically/', shell = True)

        currentRev = 'current revision:' + p.stdout.read().strip()
    except:
        currentRev = 'unable to get current revision'

    results = {'success':True, 'revision':currentRev}
    json = simplejson.dumps(results)
    return HttpResponse(json, mimetype='application/json')


def do(request, device_id, cmd_id, learn_admin, rest):
    global cmds

    results = {'success':True}

    try:
        cmd = Command.objects.get(pk=cmd_id)
    except:
        try:
            if int(cmd_id) <= 100:
                cmd = Command(cmd = 'DIM', argInt = cmd_id)
        except:
            if rest:
                results = {'success':False}
                json = simplejson.dumps(results)
                return HttpResponse(json, mimetype='application/json')
            else:
                raise Http404
            
        

    try:
        device = Device.objects.get(pk=device_id)
    except:
        if rest:
            results = {'success':False}
            json = simplejson.dumps(results)
            return HttpResponse(json, mimetype='application/json')
        else:
            raise Http404
            
    if debug:
        print 'do(), device', device_id, 'cmd_id', cmd_id

    device.execute(cmd)

    if learn_admin:
        return redirect('/core/learn/admin/')

    if rest:
        json = simplejson.dumps(results)
        return HttpResponse(json, mimetype='application/json')

    if int(cmd_id) == cmds['LEARN']:
        return redirect('/core/learn/')

    return redirect('/core/')

def learn(request, admin = False):
    global cmds
    
    devList = RawTellstickDevice.objects.all().order_by('order')
    
    if admin:
        template = 'admin/core_learn.html'
    else:
        template = 'core/learn.html'

    return render_to_response(template, {'devList': devList, 'viewType': getViewType(request), 'cmds': cmds})

@csrf_exempt
def restdo(request):
    if request.method == 'GET':
        return HttpResponseBadRequest()

    results = {'success':True}

    try:
        if debug:
            print request.POST['type']

        if request.POST['type'] == 'COMMAND':
            cmd_id = request.POST['commandid']
            device_id = request.POST['deviceid']

            try:
                cmd = Command.objects.get(pk=cmd_id)
            except:
                try:
                    if int(cmd_id) <= 100:
                        cmd = Command(cmd = 'DIM', argInt = cmd_id)
                except:
                    raise Http404
            
            try:
                device = Device.objects.get(pk=device_id)
            except:
                raise Http404
            
            if debug:
                print 'do(), device', device_id, 'cmd_id', cmd_id

            device.execute(cmd)
            
        elif request.POST['type'] == 'SIGNAL':
            sigcontent = request.POST['signal']
            try:
                sigsource = request.POST['source']
            except:
                sigsource = 'http_post'

            postToQueue(sigcontent, sigsource)

        elif request.POST['type'] == 'CREATEEVENT':
            cmd_id = request.POST['commandid']
            device_id = request.POST['deviceid']
            dotime = request.POST['dotime']
            dodate = request.POST['dodate']

            if debug:
                print 'DoTime', dotime
                print 'DoDate', dodate

            try:
                condition = request.POST['condition_globalvariableid']
            except:
                condition = None

            try:
                device = Device.objects.get(pk=device_id)
            except:
                results['message'] = 'Invalid deviceid'
                raise ValueError

            try:
                command = Command.objects.get(pk=cmd_id)
            except:
                results['message'] = 'Invalid commandid'
                raise ValueError

            try:
                local = timezone(settings.TIME_ZONE)
                doAt = local.localize(datetime.datetime.strptime(dodate.strip() + ' ' + dotime.strip(), '%Y-%m-%d %H:%M:%S'))
            except:
                try:
                    doAt = local.localize(datetime.datetime.strptime(dodate.strip() + ' ' + dotime.strip(), '%Y-%m-%d %H:%M'))
                except:
                    results['message'] = 'Invalid dodate/dotime'
                
            if doAt < local.localize(datetime.datetime.now()):
                results['message'] = 'Cant schedule events in the past'
                raise ValueError

            if debug:
                print 'CREATEEVENT'
                print command
                print device
                print dotime
                print dodate
                print doAt

            e = Event(device = device, command = command, doAt = doAt, condition = condition, schedule = None)
            e.save()

            results['eventid'] = e.id
            if debug:
                print 'Event created with id', e.id

        elif request.POST['type'] == 'CANCELEVENT':
            if debug:
                print 'cancelevent'
            event = get_object_or_404(Event, pk=request.POST['eventid'])
            if event.schedule:
                event.schedule.scheduleNext(event.doAt)
            else:
                event.delete()

        elif request.POST['type'] == 'DELAYEVENT':
            if debug:
                print 'delayevent'
            event = get_object_or_404(Event, pk=request.POST['eventid'])
            event.delay(int(request.POST['time']))
            event.save()
        else:
            results = {'success':False}

    except:
        if debug:
            strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            for s in strings:
                print s
        results['success'] = False
        
    if debug:
        print 'outcome', results['success']

    try:
        redirecturl = request.POST['redirect']
        print 'Redirect to', redirecturl
        return HttpResponseRedirect(redirecturl)
    except:
        json = simplejson.dumps(results)
        return HttpResponse(json, mimetype='application/json')

def restdoc(request):
    return render_to_response('admin/core_restdoc.html', {'viewType': getViewType(request)})

def globalvariable(request, gv_id = None):
    results = {'success':True}
    try:
        if gv_id == None:
            gvs = GlobalVariable.objects.all()
        else:
            gvs = GlobalVariable.objects.filter(pk = gv_id)

        if len(gvs) == 0:
            results = {'success':False, 'reason': 'cant find identifier'}            
        else:
            for gv in gvs:
                old, lastUpdate = gv.getOld()
                results[gv.id] = {'value': gv.getValue(),
                                  'name': gv.name,
                                  'unit': gv.unit,
                                  'hidden': gv.hidden,
                                  'key': 'GV' + str(gv.id),
                                  'datatype': gv.dataType,
                                  'old': old,
                                  'lastupdate': lastUpdate.isoformat(' ')}

    except:
        results = {'success':False, 'reason':'exception'}

    json = simplejson.dumps(results)
    return HttpResponse(json, mimetype='application/json')

def devicestate(request, dev_id = None):
    results = {'success':True}
    try:
        if dev_id == None:
            dvs = CurrentValue.objects.all()
        else:
            dvs = CurrentValue.objects.filter(key = 'DV' + str(dev_id))

        if len(dvs) == 0:
            results = {'success':False, 'reason': 'cant find identifier'}            
        else:
            for dv in dvs:
                if dv.key.startswith('DV'):
                    dev_id = dv.key[2:]
                    results[dev_id] = {'value': dv.value,
                                       'key': dv.key,
                                       'dataType': dv.dataType,
                                       'lastUpdated': dv.lastUpdated.isoformat(' ')}

    except:
        results = {'success':False, 'reason':'exception'}

    json = simplejson.dumps(results)
    return HttpResponse(json, mimetype='application/json')

def events_rest(request):
    results = {'success':True}

    local = timezone(settings.TIME_ZONE)

    try:
        events = Event.objects.all().order_by('doAt')
        results['events'] = []
        for e in events:
            timeLeft = e.doAt.astimezone(local) - local.localize(datetime.datetime.now())
            if debug:
                print timeLeft
            timeLeftStr = ''
            if timeLeft.days > 0:
                timeLeftStr = 'in %d days (at %s)' %(timeLeft.days, e.doAt.astimezone(local).strftime('%H:%M'))
            else:
                _h = int(timeLeft.seconds / 3600)
                _m = int((timeLeft.seconds - _h*3600)/60)
                _s = timeLeft.seconds - _h*3600 - _m*60
                if timeLeft.seconds >= 3600*5:
                    timeLeftStr = 'in %d hours (at %s)' %(_h, e.doAt.astimezone(local).strftime('%H:%M'))
                elif timeLeft.seconds >= 3600:                    
                    timeLeftStr = 'in %d hours %d minutes' %(_h, _m)
                elif timeLeft.seconds < 60:
                    timeLeftStr = 'any time now'
                else:
                    timeLeftStr = 'in %d minutes' %(_m)

            _t = {'doat': e.doAt.astimezone(local).strftime('%Y-%m-%d %H:%M'),
                  'doin': timeLeftStr,
                  'deviceid': e.device.id,
                  'device': e.device.name,
                  'commandid': e.command.id,
                  'eventid': e.id,
                  'command': e.command.__unicode__()}            
            
            results['events'].append(_t)
    except:
        results = {'success':False, 'reason':'exception'}
        raise

    json = simplejson.dumps(results)
    return HttpResponse(json, mimetype='application/json')
            

def plot(request, key):
    axisColor = 'white'
    if not MATPLOTLIB_AVAILABLE:
        print 'Matplotlib not available'
        raise Http404
    local = timezone(settings.TIME_ZONE)

    fromTime = local.localize(datetime.datetime.now().replace(minute = 0, second = 0, microsecond = 0)) - datetime.timedelta(hours = 24)

    valuesMem = ValueHistoryMem.objects.filter(key = key).order_by('timestamp')

    if len(valuesMem) > 0:        
        toTime = valuesMem[0].timestamp.replace(minute = 0, second = 0, microsecond = 0)
    else:
        toTime = local.localize(datetime.datetime.now())

    values = ValueHistory.objects.filter(key = key).filter(timestamp__lt = toTime).filter(timestamp__gte = fromTime)

    if debug:
        print 'FromTime', fromTime
        print 'ToTime', toTime.astimezone(local)
        print 'Earliest time in mem', valuesMem[0].timestamp.astimezone(local)
        print 'Avg-values', len(values)
        print 'Values', len(valuesMem)


    try:
        fig=Figure(figsize = (4,3), dpi = 75, facecolor = 'none')
        ax=fig.add_subplot(111)
        x=[]
        y=[]

        ymax = None
        ymin = None

        if debug:
            print 'WARNING adding debug data to plot'
            xv = datetime.datetime(2012, 11, 3, 0, 0)
            print 'xv', xv
            x.append(xv)
            y.append(3.0)
            

        for v in values:
            if debug:
                print 'Avg-value', v.timestamp, v.value

            try:
                yval = float(v.value)
                x.append(v.timestamp.replace(tzinfo = None))
                y.append(yval)
            except:
                print 'Failed adding value to plot,', repr(v.value), repr(v.timestamp)
                continue

            if ymax == None:
                ymax = yval
            if ymin == None:
                ymin = yval

            ymax = max(ymax, yval)
            ymin = min(ymin, yval)
        

        for v in valuesMem:
            if debug:
                print 'Value', v.timestamp, v.value

            try:
                yval = float(v.value)
                x.append(v.timestamp.replace(tzinfo = None))
                y.append(yval)
            except:
                print 'Failed adding mem value to plot,', repr(v.value), repr(v.timestamp)
                continue

            if ymax == None:
                ymax = yval
            if ymin == None:
                ymin = yval

            ymax = max(ymax, yval)
            ymin = min(ymin, yval)

        if ymax == None:
            ymax = 1.0
        if ymin == None:
            ymin = 0.0

        if debug:
            print 'detected ymax, ymin', ymax, ymin
        if ymax >= 0:
            ymax = float(int(ymax)) + 1.0
        else:
            ymax = float(int(ymax))

        if ymin >= 0:
            ymin = float(int(ymin))
        else:
            ymin = float(int(ymin)) - 1.0

        if debug:
            print 'fixed ymax, ymin', ymax, ymin


        ax.plot_date(x, y, '-')# , tz = local)
        ax.xaxis.set_major_formatter(DateFormatter('%H:%M'))
        ax.xaxis.set_major_locator(MaxNLocator(5))
        ax.set_xlim(fromTime, datetime.datetime.now())
        ax.set_ylim(ymin, ymax)
        ax.grid(True, which='both')
        ax.spines['bottom'].set_color(axisColor)
        ax.spines['top'].set_color(axisColor)
        ax.spines['left'].set_color(axisColor)
        ax.spines['right'].set_color(axisColor)
        ax.xaxis.label.set_color(axisColor)
        ax.yaxis.label.set_color(axisColor)
        ax.tick_params(axis='x', colors=axisColor)
        ax.tick_params(axis='y', colors=axisColor)
        fig.autofmt_xdate()

        canvas = FigureCanvas(fig)
        response = HttpResponse(content_type='image/png')
        canvas.print_png(response)

    except:
        strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
        for s in strings:
            print s
        
        raise Http404

    return response


def event_index(request, admin):
    events = Event.objects.all().order_by('doAt')

    if admin:
        template = 'admin/core_event_index.html'
    else:
        template = 'core/event_index.html'
    
    return render_to_response(template, {'viewType': getViewType(request), 'events': events})


def event(request, event_id, admin):
    event = get_object_or_404(Event, pk=event_id)

    if admin:
        template = 'admin/core_event_detail.html'
    else:
        template = 'core/event_detail.html'

    return render_to_response(template, {'viewType': getViewType(request), 'event': event})

def delay_event(request, event_id, minutes, admin):
    if debug:
        print 'Delay Event', event_id
    event = get_object_or_404(Event, pk=event_id)
    event.delay(int(minutes))
    event.save()

    if admin:
        redir = '/core/events/admin/'
    else:
        redir = '/core/events/'

    return redirect(redir)

def cancel_event(request, event_id, admin):
    event = get_object_or_404(Event, pk=event_id)
    if event.schedule:
        event.schedule.scheduleNext(event.doAt)
    else:
        event.delete()

    if admin:
        redir = '/core/events/admin/'
    else:
        redir = '/core/events/'

    return redirect(redir)




def addDevice(id, name, protocol, model, house, unit, code):
    existing = RawTellstickDevice.objects.filter(house = house, unit = unit, code = code)

    if existing.count() == 0:
        if debug:
            print 'Create new RawTellstickDevice'            
        dev = RawTellstickDevice(deviceId = id,
                        name = name,
                        rawName = name,
                        protocol = protocol + ';' + model,
                        house = house,
                        unit = unit,
                        code = code)        
        print dev
        try:
            dev.clean()
            dev.save()
        except:
            print 'Error saving this device'
            strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            for s in strings:
                print s


    elif existing.count() >= 1:
        if debug:
            print 'Update existing RawTellstickDevice'
        dev = existing[0]

        dev.deviceId = id

        if dev.rawName == dev.name:
            dev.name = name

        dev.rawName = name
        dev.protocol = protocol + ';' + model

        try:
            dev.clean()
            dev.save()
        except:
            print 'Error saving this device'
            strings = traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            for s in strings:
                print s

def dumpDev(dev):
    t = {}
    t['deviceId'] = dev.id
    t['name'] = dev.name
    t['order'] = dev.order
    t['hidden'] = dev.hidden
    s = {}
    if dev.activate:
        s['activate'] = cmds['ACTIVATE']
    if dev.onOff:
        s['on'] = cmds['ON']
        s['off'] = cmds['OFF']
    if dev.dim:
        s['dim'] = '0-100'
        
    t['supportedCommands'] = s
    return t


def rest_devices(request):
    #Need to build this to hold all information in devices and subclasses
    to_json = []

    for r in RawTellstickDevice.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'raw'
        to_json.append(t)
        
    for r in GroupDevice.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'group'
        to_json.append(t)

    for r in TimerDevice.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'timer'
        to_json.append(t)

    for r in Preset.objects.all():        
        t = dumpDev(r)
        t['deviceType'] = 'preset'
        to_json.append(t)
  
    to_json.sort(key=lambda dev: dev['order'])

    json_serializer = serializers.get_serializer("json")()

    return HttpResponse(simplejson.dumps(to_json, indent = 2), mimetype="application/json")


class BackupUploadForm(forms.Form):
    file  = forms.FileField()
    


def niceDate(d):
    parts = d.split('_')
    return parts[0] + ' ' + parts[1][0:2] + ':' + parts[1][2:4] + ':' + parts[1][4:6]

def backup(request):

    if request.method == 'POST':
        form = BackupUploadForm(request.POST, request.FILES)
        if form.is_valid():
            if request.FILES['file'].name.endswith('.tgz'):
                with open(os.path.join(settings.DB_BACKUP_ROOT, request.FILES['file'].name), 'wb+') as destination:
                    for chunk in request.FILES['file'].chunks():
                        destination.write(chunk)

                cmd = 'tar xvfz ' + request.FILES['file'].name
                p =subprocess.Popen(cmd, stdout = subprocess.PIPE, cwd = settings.DB_BACKUP_ROOT, shell = True)
                print p.stdout.read()
                
                cmd = 'rm ' + request.FILES['file'].name
                p =subprocess.Popen(cmd, stdout = subprocess.PIPE, cwd = settings.DB_BACKUP_ROOT, shell = True)
                print p.stdout.read()
                
            else:
                msg = 'Bad file uploaded, should end with: .tgz'
            
    else:
        form = BackupUploadForm()

    files = os.listdir(settings.DB_BACKUP_ROOT)
    backups = {}
    for f in files:
        if len(f) < 5:
            continue

        if f[-3:] == '.db':
            d = f[:-3]
            if d not in backups:
                backups[d] = {'date': niceDate(d), 'backupFile': f[:-3]}
        if f[-4:] == '.rev':
            d = f[:-4]
            if d not in backups:
                backups[d] = {'date': niceDate(d), 'backupFile': f[:-4]}

            fd = open(os.path.join(settings.DB_BACKUP_ROOT, f), 'r')
            rev = fd.read()
            fd.close()

            backups[d]['rev'] = rev.strip()

        if f[-4:] == '.tgz':
            d = f[21:-4]
            if d not in backups:
                backups[d] = {'date': niceDate(d), 'backupFile': f[:-4]}

            backups[d]['download'] = f
            
        print f

    print backups.keys()

    items = []

    if len(backups) > 0:
        keys = backups.keys()
        keys.sort()
        keys.reverse()
        for k in keys:
            print k
            items.append(backups[k])

    p =subprocess.Popen('git rev-list --max-count=1 HEAD', stdout = subprocess.PIPE, cwd = '/home/pi/source/automagically/', shell = True)

    currentRev = p.stdout.read().strip()

    return render(request, 'admin/core_backup.html', {'items': items, 'currentRev': currentRev, 'form': form})
    
    
def doBackup(request):
    basefile = os.path.splitext(datetime.datetime.now().isoformat('_'))[0].replace(':', '')
    basepath = os.path.join(settings.DB_BACKUP_ROOT, basefile)
    dumpFile = basepath + '.db'
    cmd = 'mysqldump --user=' + settings.DATABASES['default']['USER'] + ' --password=' + settings.DATABASES['default']['PASSWORD'] + ' ' + settings.DATABASES['default']['NAME'] + ' --add-drop-database --result-file=' + dumpFile
    print 'Executing:', cmd
    subprocess.Popen(cmd, shell = True).wait()

    p =subprocess.Popen('git rev-list --max-count=1 HEAD', stdout = subprocess.PIPE, cwd = '/home/pi/source/automagically/', shell = True)

    fd = open(basepath + '.rev', 'w')
    fd.write(p.stdout.read())
    fd.write('\n')
    fd.close()

    cmd = 'tar czvf ' + 'automagically_backup_' + basefile + '.tgz ' + basefile + '*'
    print 'Executing', cmd
    p =subprocess.Popen(cmd, stderr = subprocess.STDOUT, stdout = subprocess.PIPE, cwd = settings.DB_BACKUP_ROOT, shell = True)

    print p.stdout.read()

    return redirect('/backup/')

def showBackup(request, backup):
    basefile = os.path.join(settings.DB_BACKUP_ROOT, backup)

    p =subprocess.Popen('git rev-list --max-count=1 HEAD', stdout = subprocess.PIPE, cwd = '/home/pi/source/automagically/', shell = True)

    currentRev = p.stdout.read().strip()

    print basefile

    print os.path.exists(basefile + '.db'), os.path.exists(basefile + '.rev')

    if os.path.exists(basefile + '.db') and os.path.exists(basefile + '.rev'):
        print 'Possible'
        state = 'POSSIBLE'
        fd = open(basefile + '.rev', 'r')
        rev = fd.read()
        fd.close()

        
    else:

        state = 'IMPOSSIBLE'
        rev = ''

    return render(request, 'admin/core_backup_show.html', {'rev': rev, 'currentRev': currentRev, 'backupDate': niceDate(backup), 'state': state, 'backup': backup})    
    

def restoreBackup(request, backup):
    print 'Restore backup', backup
    backupFile = os.path.join(settings.DB_BACKUP_ROOT, backup) + '.db'

    if os.path.exists(backupFile):
        postToQueue('system,dbrestore:' + backupFile , 'web')
    else:
        print 'Backupfile does not exist'
        return redirect('/backup/')

    return redirect('/backup/done/')

def backupDone(request):
    postToQueue('system,reboot', 'web')
    return render(request, 'admin/core_backup_done.html', {})

def doUpdate(request):
    postToQueue('system,update', 'web')
    return redirect('/update/done/')

def updateDone(request):
    return render(request, 'admin/core_update_done.html', {})

def viewLogs(request):
    
    try:
        f = os.path.join(settings.LOG_ROOT, 'init.cmd.log')
        fd = open(f, 'r')
        initLog = fd.read()
        fd.close()        
        initLogDate = datetime.datetime.fromtimestamp(os.path.getmtime(f))

    except:
        initLog = ''
        initLogDate = ''

    try:
        f = os.path.join(settings.LOG_ROOT, 'update.log')
        fd = open(f, 'r')
        updateLog = fd.read()
        fd.close()
        updateLogDate = datetime.datetime.fromtimestamp(os.path.getmtime(f))
    except:
        updateLog = ''
        updateLogDate = ''
    

    return render(request, 'admin/core_view_logs.html', {'init': initLog,
                                                         'initDate': initLogDate,
                                                         'update': updateLog,
                                                         'updateDate': updateLogDate})

    
def read_config(request):

    count = 0

    fd = open('/etc/tellstick.conf')

    lines = fd.readlines()

    id = ''
    name = ''
    protocol = ''
    model = ''
    house = ''
    unit = ''
    code = ''

    for i in range(len(lines)):
        if lines[i].strip().startswith('device {'):
            if id != '':
                count += 1
                addDevice(id, name, protocol, model, house, unit, code)
            id = ''
            name = ''
            protocol = ''
            model = ''
            house = ''
            unit = ''
            code = ''
            if debug:
                print 'new device'

        elif lines[i].strip().startswith('id = '):
            id = lines[i].split('=')[1].strip()
        elif lines[i].strip().startswith('name = '):
            name = lines[i].split('"')[1]
        elif lines[i].strip().startswith('protocol = '):
            protocol = lines[i].split('"')[1]
        elif lines[i].strip().startswith('model = '):
            model = lines[i].split('"')[1].split(':')[0]
        elif lines[i].strip().startswith('parameters {'):
            pass
        elif lines[i].strip().startswith('house = '):
            house = lines[i].split('"')[1]
        elif lines[i].strip().startswith('unit = '):
            unit = lines[i].split('"')[1]
        elif lines[i].strip().startswith('code = '):
            code = lines[i].split('"')[1]
        elif lines[i].strip().startswith('#'):
            pass

    if id != '':
        count += 1
        addDevice(id, name, protocol, model, house, unit, code)

    print '/etc/tellstick.conf contains', count, 'devices that has been processe'
    return redirect('/core/')

    
